# coding=utf-8
from wtforms import SelectField, StringField, TextField, SubmitField
from wtforms.validators import input_required
from wtforms.widgets import TextArea
from faan.core.model.ticket import Ticket
from faan.web.forms import XForm
from faan.web.forms.widgets import BSTextInput, BSTextArea, BSSelect, BSSubmit

__author__ = 'limeschnaps'


class TicketForm(XForm):
    # Categories hr mapping
    _category_mapping = {
        "GENERAL": u"Общие вопросы",
        "TECHNIC": u"Технические вопросы",
        "FINANCIAL": u"Финансовые вопросы",
    }

    # Urgency hr mapping
    _urgency_mapping = {
        "LOW": u"Низкая",
        "MEDIUM": u"Средняя",
        "HIGH": u"Высокая",
        "CRITICAL": u"Критическая",
    }

    title = StringField(label=u"Заголовок", validators=[input_required()], widget=BSTextInput())

    category = SelectField(label=u"Категория",
                           coerce=int,
                           validators=[input_required()],
                           widget=BSSelect(),
                           choices=[(k, _category_mapping.get(v)) for k, v in Ticket.Category.inverse().iteritems()])

    urgency = SelectField(label=u"Срочность",
                          coerce=int,
                          validators=[input_required()],
                          widget=BSSelect(),
                          choices=[(k, _urgency_mapping.get(v)) for k, v in Ticket.Urgency.inverse().iteritems()])

    message = TextField(label=u"Текст запроса", validators=[input_required()], widget=BSTextArea())

    submit = SubmitField(label=u"Отправить", widget=BSSubmit())


class TicketReplyForm(XForm):
    message = TextField(label=u"Ответ", validators=[input_required()], widget=BSTextArea())
    submit = SubmitField(label=u"Ответить", widget=BSSubmit())