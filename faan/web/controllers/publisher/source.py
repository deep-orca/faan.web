# coding=utf-8
from flask import request, g, redirect, jsonify
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.core.X.xflask import (
    Controller,
    Route,
    capp
)
from faan.web.lib.security import (
    IsSigned,
    NotOwnedException,
    NotAllowedException
)
from faan.core.model.meta import Session
from faan.core.model.pub.application import Application

from faan.core.model.pub.unit import Unit
from faan.core.model.pub.source import Source
from faan.core.X.helpers.source import SourceHelper, SourceHelperFormObject
from faan.web.forms.publisher import (
    SourceBannerVidigerForm,
    SourceInterstitialVidigerForm,
    SourceBannerAdMobForm,
    SourceInterstitialAdMobForm
)

source_helper = SourceHelper([
    SourceHelperFormObject(Source.Type.VIDIGER, Unit.Type.BANNER, SourceBannerVidigerForm),
    SourceHelperFormObject(Source.Type.VIDIGER, Unit.Type.INTERSTITIAL, SourceInterstitialVidigerForm),
    SourceHelperFormObject(Source.Type.ADMOB, Unit.Type.BANNER, SourceBannerAdMobForm),
    SourceHelperFormObject(Source.Type.ADMOB, Unit.Type.INTERSTITIAL, SourceInterstitialAdMobForm),
])

MESSAGES = {
    'default': (u'Этот источник создан по умолчанию. Вы можете изменить настройки или оставить все как есть.', 'warning')
}

@CProtect(IsSigned())
@Controller('/pub/application/<int:app_id>/unit/<int:unit_id>/source')
class UnitSourceController(object):
    @Route('/')
    def list_view(self, app_id, unit_id):
        account_id = request.environ['REMOTE_USER']

        # app_id = 9786676  # App ID mismatch test
        # unit_id = 9786676  # Unit ID mismatch test

        app = Session.query(Application).filter(Application.id == app_id, Application.account == account_id).first()
        if not app: raise NotOwnedException()

        unit = Session.query(Unit).filter(Unit.id == unit_id, Unit.application == app_id).first()
        if not unit: raise NotOwnedException()

        sources = Session.query(Source).filter(Source.unit == unit_id).all()

        # source_types = {v: False for k, v in Source.Type.iteritems()}
        source_types = []
        for source in sources:
            source_types.append(source.type)

        g.has_source_type = lambda t: t in list(set(source_types))

        g.application = app
        g.unit = unit
        g.sources = sources

        return render_template('/pub/source/list.mako')

    @Route('/new', methods=['GET', 'POST'])
    def create_view(self, app_id, unit_id):
        account_id = request.environ['REMOTE_USER']

        g.message = MESSAGES.get(request.values.get('message'))

        try:
            source_type = Source.Type[request.values.get('type')]
        except Exception, e:
            source_type = Source.Type.VIDIGER

        # app_id = 9786676  # App ID mismatch test
        # unit_id = 9786676  # Unit ID mismatch test

        app = Session.query(Application).filter(Application.id == app_id, Application.account == account_id).first()
        if not app: raise NotOwnedException()

        unit = Session.query(Unit).filter(Unit.id == unit_id, Unit.application == app_id).first()
        if not unit: raise NotOwnedException()

        SourceCls, FormCls = source_helper.get(source_type, app.os, unit.type)

        source = SourceCls.Default()

        # Piece of blue tape for Vitaly
        if source_type == Source.Type.VIDIGER:
            source.media_types = [1, 2, 3]
        # End

        form = FormCls(request.form, obj=source)

        if request.method == 'POST' and form.validate():
            # Populate common fields
            source.type = source_type
            source.unit = unit.id
            source.state = Source.State.ACTIVE

            # Populate form dependent fields
            form.populate_obj(source)

            try:
                Session.flush()
                Session.add(source)
                Session.commit()

                return redirect('/pub/application/{app_id}/unit/{unit_id}/source'.format(app_id=app_id, unit_id=unit_id))
            except Exception, e:
                capp.logger.exception(e)

        g.app = app
        g.unit = unit
        g.source = source

        return render_template('/pub/source/form.mako', form=form, partial=form.template)

    @Route('/<int:source_id>', methods=['GET', 'POST'])
    def edit_view(self, app_id, unit_id, source_id):
        account_id = request.environ['REMOTE_USER']

        g.message = MESSAGES.get(request.values.get('message'))

        # app_id = 9786676  # App ID mismatch test
        # unit_id = 9786676  # Unit ID mismatch test

        app = Session.query(Application).filter(Application.id == app_id, Application.account == account_id).first()
        if not app: raise NotOwnedException()

        unit = Session.query(Unit).filter(Unit.id == unit_id, Unit.application == app_id).first()
        if not unit: raise NotOwnedException()

        source = Session.query(Source).filter(Source.unit == unit_id, Source.id == source_id).first()
        if not source: raise NotOwnedException()

        SourceCls, FormCls = source_helper.get(source.type, app.os, unit.type)

        form = FormCls(request.form, obj=source)

        if request.method == 'POST' and form.validate():
            form.populate_obj(source)

            try:
                Session.flush()
                Session.add(source)
                Session.commit()

                return redirect('/pub/application/{app_id}/unit/{unit_id}/source'.format(app_id=app_id, unit_id=unit_id))
            except Exception, e:
                capp.logger.exception(e)

        g.app = app
        g.unit = unit
        g.source = source

        return render_template('/pub/source/form.mako', form=form, partial=form.template)

    @Route("/<int:source_id>/delete")
    def delete(self, app_id, unit_id, source_id):
        account_id = request.environ['REMOTE_USER']

        app = Session.query(Application).filter(Application.id == app_id, Application.account == account_id).first()
        if not app: raise NotOwnedException()

        unit = Session.query(Unit).filter(Unit.id == unit_id, Unit.application == app_id).first()
        if not unit: raise NotOwnedException()

        source = Session.query(Source).filter(Source.unit == unit_id, Source.id == source_id).first()
        if not source: raise NotOwnedException()

        try:
            Session.delete(source)
            Session.flush()
            Session.commit()
        except Exception, e:
            capp.logger.exception("While deleting unit")

        return redirect("/pub/application/{0}/unit/{1}/source".format(app_id, unit_id))

    @Route("/<int:source_id>/<string:state>")
    def state(self, app_id, unit_id, source_id, state):
        account_id = request.environ['REMOTE_USER']

        app = Session.query(Application).filter(Application.id == app_id, Application.account == account_id).first()
        if not app: raise NotOwnedException()

        unit = Session.query(Unit).filter(Unit.id == unit_id, Unit.application == app_id).first()
        if not unit: raise NotOwnedException()

        source = Session.query(Source).filter(Source.unit == unit_id, Source.id == source_id).first()
        if not source: raise NotOwnedException()

        states = {
            'activate': Source.State.ACTIVE,
            'deactivate': Source.State.DISABLED,
        }

        state = states.get(state)

        print state

        if state not in [Source.State.ACTIVE, Source.State.DISABLED]:
            raise NotAllowedException()

        source.state = state

        try:
            Session.flush()
            Session.add(source)
            Session.commit()
        except Exception, e:
            print e
            capp.logger.exception("While setting source state")

        return redirect("/pub/application/{0}/unit/{1}/source".format(app_id, unit_id))