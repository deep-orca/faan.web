# coding=utf-8
import datetime
import time
import operator
import traceback

from flask import request, g, redirect
from sqlalchemy.sql.functions import count
from sqlalchemy.util import KeyedTuple

from faan.core.X.xflask import Controller, Route, capp
from faan.core.model.ticket import Ticket, TicketReply
from faan.core.model.meta import Session
from faan.core.X.mako import render_template
from faan.web.app import account
from faan.web.forms.ticket import TicketForm, TicketReplyForm
from faan.web.lib.security import IsSigned, NotOwnedException
from faan.x.security import CProtect


__author__ = 'limeschnaps'

from sqlalchemy.ext.declarative import DeclarativeMeta


class SAJson(object):
    @staticmethod
    def serialize(query, sort=None, ts_fields=None):
        cls = None
        json_ready_query = []
        for obj in query:
            if isinstance(obj.__class__, DeclarativeMeta):
                if not cls:
                    cls = obj.__class__

                json_ready_obj = dict((c, getattr(obj, c)) for c in [x.name for x in cls.__table__.columns])
            elif isinstance(obj, KeyedTuple):
                json_ready_obj = dict(zip(obj.keys(), obj))

            if ts_fields:
                for field in ts_fields:
                    if field in json_ready_obj:
                        json_ready_obj[field] = int("%d000" % json_ready_obj[field])

            json_ready_query.append(json_ready_obj)

        if sort:
            field, order = sort
            json_ready_query = sorted(json_ready_query, key=operator.itemgetter(field))
            if order == -1:
                json_ready_query.reverse()

        return json_ready_query


def get_total_new_replies():
    return dict(Session()
                .query(Ticket.state, count(TicketReply.id))
                .join(TicketReply)
                .filter(
                    Ticket.account_id == account.id,
                    TicketReply.account_id != account.id,
                    Ticket.ts_viewed_owner < TicketReply.ts_created
                )
                .group_by(Ticket.state).all())


def get_new_replies(tickets):
    id_list = [t.id for t in tickets]
    return dict(Session()
                .query(Ticket.id, count(TicketReply.id)).join(TicketReply).filter(
                    Ticket.id.in_(id_list),
                    TicketReply.account_id != account.id,
                    Ticket.ts_viewed_owner < TicketReply.ts_created
                ).group_by(Ticket.id).all())



@CProtect(IsSigned())
@Controller("/<account_type>/support")
class SupportController(object):
    @Route("/")
    @Route("/<string:view_type>")
    def list_ticket_view(self, account_type, view_type="open"):
        """Shows a list of tickets depending on view type

        template_name :: support/list.mako

        view_type -- [open|resolved] (default `open`)
        """
        print request

        tickets = Session.query(Ticket).filter(Ticket.account_id == account.id)
        if view_type == "open":
            tickets = tickets.filter(Ticket.state == Ticket.State.OPEN)
        elif view_type == "resolved":
            tickets = tickets.filter(Ticket.state == Ticket.State.RESOLVED)

        tickets = tickets.order_by(-Ticket.id).all()

        # Setting context
        g.total_new_replies = get_total_new_replies()
        g.tickets = tickets
        g.view_type = view_type

        return render_template("support/list.mako", account_type=account_type)

    @Route("/<int:ticket_id>", methods=["GET", "POST"])
    def detail_ticket_view(self, account_type, ticket_id=None):
        """Showing selected ticket
        Raises `NotOwnedException` if opened by not owner user

        template_name :: support/details.mako
        form_class :: TicketReplyForm
        """
        print request

        ticket = Ticket.Single(account_id=account.id, id=ticket_id)

        if not ticket:
            raise KeyError

        if ticket.account.id != account.id:
            raise NotOwnedException

        ticket.ts_viewed_owner = int(time.mktime(datetime.datetime.utcnow().timetuple()))  # Viewed by owner now
        try:
            Session.add(ticket)
            Session.flush()
            Session.commit()
        except:
            Session.rollback()
            capp.logger.exception("While marking a reply as read")

        form = TicketReplyForm()

        if request.method == "POST" and form.validate():
            reply = TicketReply.Default()
            form.populate_obj(reply)
            reply.ts_created = int(time.mktime(datetime.datetime.utcnow().timetuple()))
            reply.account_id = account.id
            reply.ticket = ticket_id

            try:
                Session.add(reply)
                Session.flush()
                Session.commit()
                return redirect("/%s/support/%s" % (account_type, ticket_id))
            except:
                Session.rollback()
                capp.logger.exception("While saving new reply")

        # Setting context
        g.ticket = ticket
        g.total_new_replies = get_total_new_replies()
        g.view_type = "open" if ticket.state == Ticket.State.OPEN else "resolved"

        return render_template("support/details.mako", account_type=account_type, form=form)

    @Route("/new", methods=["GET", "POST"])
    def create_ticket_view(self, account_type):
        """Creates new ticket

        template_name :: support/form.mako
        form_class :: TicketForm
        """
        print request

        ticket = Ticket.Default()

        form = TicketForm(request.form, obj=ticket)
        if request.method == "POST" and form.validate():
            form.populate_obj(ticket)
            ts_now = int(time.mktime(datetime.datetime.utcnow().timetuple()))
            ticket.ts_created = ts_now          # Created now
            ticket.ts_viewed_owner = ts_now     # Viewed by owner now
            ticket.ts_viewed_support = 0        # Never viewed by support (0)
            ticket.account_id = account.id
            ticket.state = Ticket.State.OPEN

            try:
                Session().add(ticket)
                Session().flush()
                Session().commit()
                return redirect("/%s/support/" % account_type)
            except:
                Session.rollback()
                capp.logger.exception("While saving new ticket")

        # Setting context
        g.account_type = account_type
        g.total_new_replies = get_total_new_replies()
        g.view_type = "new"

        return render_template("support/form.mako", form=form, account_type=account_type)

    @Route("/<int:ticket_id>/resolve")
    def resolve_ticket_view(self, account_type, ticket_id=None):
        """Marking ticket as resolved

        ticket_id -- Ticket ID (default None)
        """
        ticket = Ticket.Single(account_id=account.id, id=ticket_id)

        if not ticket:
            raise KeyError

        if ticket.account.id != account.id:
            raise NotOwnedException

        ticket.state = Ticket.State.RESOLVED

        try:
            Session.add(ticket)
            Session.flush()
            Session.commit()
        except:
            Session.rollback()
            capp.logger.exception("While resolving ticket")
            return redirect("/%s/support/%s" % (account_type, ticket_id))

        return redirect("/%s/support" % account_type)

