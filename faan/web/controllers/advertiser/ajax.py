# -*- coding: utf-8 -*-
from pprint import pprint

from flask import g, request, redirect, make_response, jsonify
from sqlalchemy.sql.expression import desc
from faan.core.X.xflask import Controller, Route, Error, capp, getCurrentIP
from faan.core.X.mako import render_template
from faan.core.model.general.geo import GeoCountry, GeoRegion, GeoCity
from faan.core.model.security import Account
from faan.web.lib.helpers import SAJson, divbyz, get_date_limits, get_ts_list, make_graph_series, format_ts, \
    graph_series_to_list, make_table_series, table_series_to_list
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned

from faan.core.model.adv.statistics import AdvStat
from faan.core.model.adv.campaign import Campaign
from faan.core.model.adv.media import Media
from faan.core.model import Category
from faan.core.model.meta import Session
import decimal
import time
import operator
from datetime import datetime
from faan.x.util.timestamp import TimeStamp


@CProtect(IsSigned())
@Controller("/adv/ajax")
class AdvAjaxController(object):
    @Route("/dashboard/table", methods=["GET"])
    def dashboard_table(self):
        user = request.environ['REMOTE_USER']

        start, end, hourly, tz = get_date_limits(request)

        q = AdvStat.PreQuery() \
            .filter(AdvStat.ts_spawn >= start) \
            .filter(AdvStat.ts_spawn < end) \
            .filter(AdvStat.account == user)

        q_table = q.add_column(AdvStat.campaign).group_by(AdvStat.campaign)
        q_table = SAJson.serialize(q_table.all(), sort=('campaign', 1), ts_fields=[])

        camp_names = {camp.id: camp.name for camp in Campaign.Filter(account=user).all()}

        for item in q_table:
            item.update({
                "name": camp_names.get(item.get("campaign")),
                "ctr": (divbyz(float(item.get("clicks")), item.get("impressions")) * 100),
                "cost": item.get("cost")
            })

        data = {
            "meta": {
                "date_from": format_ts(start),
                "date_to": format_ts(end)
            },
            "objects": q_table
        }

        return jsonify(**data)

    @Route("/dashboard/graph/overview", methods=["GET"])
    def dashboard_graph_overview(self):
        user = request.environ['REMOTE_USER']

        start, end, hourly, tz = get_date_limits(request)
        grouping = AdvStat.Presets.GROUP_HOURLY if hourly else AdvStat.Presets.GROUP_DAILY_TZ(tz)

        query = AdvStat.PreQuery() \
            .filter(AdvStat.ts_spawn >= start) \
            .filter(AdvStat.ts_spawn < end) \
            .filter(AdvStat.account == user)

        query = query.add_column(grouping.label('ts_spawn')).group_by(grouping)

        query_result = query.all()

        ts_list = get_ts_list(request)

        graphs = {gname: make_graph_series(ts_list) for gname in ["cost", "impressions", "clicks"]}

        for x in query_result:
            for gname, gs in graphs.iteritems():
                if start <= x.ts_spawn <= end:
                    graphs[gname][x.ts_spawn-(tz if not hourly else 0)] = float(getattr(x, gname))

        data = {
            "meta": {
                "hourly": hourly,
                "date_from": format_ts(start),
                "date_to": format_ts(end)
            },
            "objects": [
                {
                    "label": u"Затраты",
                    "data": graph_series_to_list(graphs["cost"], tz, hourly)
                },
                {
                    "label": u"Просмотров",
                    "data": graph_series_to_list(graphs["impressions"], tz, hourly)
                },
                # {
                #     "label": u"Завершенных просмотров",
                #     "data": graph_series_to_list(graphs["impressions"], tz, hourly)
                # },
                {
                    "label": u"Кликов",
                    "data": graph_series_to_list(graphs["clicks"], tz, hourly)
                }
            ]
        }

        return jsonify(**data)

    @Route("/dashboard/graph/cost", methods=["GET"])
    def dashboard_graph_cost(self):
        user = request.environ['REMOTE_USER']

        start, end, hourly, tz = get_date_limits(request)

        grouping = AdvStat.Presets.GROUP_HOURLY if hourly else AdvStat.Presets.GROUP_DAILY_TZ(tz)

        query = AdvStat.PreQuery() \
            .filter(AdvStat.ts_spawn >= start) \
            .filter(AdvStat.ts_spawn < end) \
            .filter(AdvStat.account == user) \
            .add_column(grouping.label('ts_spawn')) \
            .add_column(AdvStat.campaign) \
            .group_by(grouping, AdvStat.campaign)

        query_result = query.all()


        camp_names = {camp.id: camp.name for camp in Campaign.Filter(account=user).all()}

        camps = {}
        for x in query_result:
            if not camps.get(x.campaign):
                camps[x.campaign] = []
            camps[x.campaign].append(x)

        ts_list = get_ts_list(request)

        obj_list = []
        for camp_id, camp_data in camps.iteritems():
            graph_series = make_graph_series(ts_list)


            # pprint(graph_series)
            for cd in sorted(camp_data, key=lambda x: x.ts_spawn):
                if start <= cd.ts_spawn <= end:
                    graph_series[cd.ts_spawn - (tz if not hourly else 0)] = float(cd.cost) #

            pprint(graph_series)
            obj_list.append({
                "label": camp_names.get(camp_id, ""),
                "data": graph_series_to_list(graph_series, tz, hourly)
            })

        data = {
            "meta": {
                "hourly": hourly,
                "date_from": format_ts(start),
                "date_to": format_ts(end)
            },
            "objects": obj_list or graph_series_to_list(make_graph_series(ts_list))
        }

        return jsonify(**data)

    @Route("/campaign", methods=["GET"])
    def camp_list(self):
        user = request.environ["REMOTE_USER"]
        obj_list = [{"id": None, "name": u"Все"}]

        search = request.values.get("q")

        q = Session.query(Campaign.id, Campaign.name) \
            .filter(Campaign.account == user)

        if search and search is not "":
            q = q.filter(Campaign.name.ilike(u"%{0}%".format(search)))

        q = q.all()

        obj_list += [{"id": c.id, "name": c.name} for c in q]

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/campaign/<int:camp_id>/media", methods=["GET"])
    def media_list(self, camp_id=0):
        user = request.environ["REMOTE_USER"]
        obj_list = [{"id": None, "name": u"Все"}]

        if camp_id:
            search = request.values.get("q")

            try:
                _camp_id = Session.query(Campaign.id).filter(Campaign.id == camp_id, Campaign.account == user).first().id
                q = Session.query(Media.id, Media.name) \
                    .filter(Media.campaign == _camp_id)

                if search and search is not "":
                    q = q.filter(Media.name.ilike(u"%{0}%".format(search)))

                q = q.all()

                obj_list += [{"id": z.id, "name": z.name} for z in q]
            except:
                pass

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/stat", methods=["GET", "POST"])
    @Route("/stat/<group>", methods=["GET", "POST"])
    def stat(self, group="date"):
        user = request.environ["REMOTE_USER"]
        group = group or "date"
        campaign = request.values.get('campaign')
        media = request.values.get('media')

        country = request.values.get('country')
        region = request.values.get('region')
        city = request.values.get('city')

        camp_names = media_names = country_names = region_names = city_names = {}

        start, end, hourly, tz = get_date_limits(request)

        q = AdvStat.PreQuery() \
            .filter(AdvStat.ts_spawn >= start) \
            .filter(AdvStat.ts_spawn < end) \
            .filter(AdvStat.account == user)

        if campaign:
            q = q.filter(AdvStat.campaign == campaign)
        if media:
            q = q.filter(AdvStat.media == media)
        if country:
            q = q.filter(AdvStat.country == country)
        if region:
            q = q.filter(AdvStat.region == region)
        if city:
            q = q.filter(AdvStat.city == city)

        if group == 'date':
            grouping = AdvStat.Presets.GROUP_HOURLY if hourly else AdvStat.Presets.GROUP_DAILY_TZ(-tz)
            q = SAJson.serialize(q.add_column(grouping.label('ts_spawn')).group_by(grouping).all())
            # table_series = make_table_series(['clicks', 'cost', 'endcard_cost', 'endcards', 'impressions', 'overlay_cost', 'overlays', 'raw'])
            # table_series.update({i.get("ts_spawn") - (tz if not hourly else 0): i for i in q})
            # q = table_series_to_list(table_series, tz=tz)
        elif group == 'campaign':
            q = q.add_column(AdvStat.campaign).group_by(AdvStat.campaign)
            q = sorted(SAJson.serialize(q.all()), key=lambda x: x.get('campaign'))
            camp_names = {camp.id: camp.name for camp in Campaign.Filter(account=user).all()}
        elif group == 'media':
            q = q.add_column(AdvStat.media).group_by(AdvStat.media)
            q = sorted(SAJson.serialize(q.all()), key=lambda x: x.get('media'))
            media_names = {camp.id: camp.name for camp in Media.Filter(account=user).all()}
        elif group == 'country':
            q = q.add_column(AdvStat.country).group_by(AdvStat.country)
            q = sorted(SAJson.serialize(q.all()), key=lambda x: x.get('country'))
            country_names = {x.id: x.name for x in GeoCountry.Filter().all()}
            country_names.update({0: u"Другое"})
        elif group == 'region':
            q = q.add_column(AdvStat.region).group_by(AdvStat.region)
            q = sorted(SAJson.serialize(q.all()), key=lambda x: x.get('region'))
            region_names = {x.id: x.name for x in GeoRegion.Filter().all()}
            region_names.update({0: u"Другое"})
        elif group == 'city':
            q = q.add_column(AdvStat.city).group_by(AdvStat.city)
            q = sorted(SAJson.serialize(q.all()), key=lambda x: x.get('city'))
            city_names = {x.id: x.name for x in GeoCity.Filter().all()}
            city_names.update({0: u"Другое"})

        for item in q:
            item.update({
                # "ts_spawn": item.get("ts_spawn") * 1000,
                "country_name": country_names.get(item.get("country")),
                "campaign_name": camp_names.get(item.get("campaign")),
                "region_name": region_names.get(item.get("region")),
                "city_name": city_names.get(item.get("city")),
                "media_name": media_names.get(item.get("media")),
                "ctr": (divbyz(float(item.get("clicks")), item.get("impressions")) * 100),
                "cpm": (divbyz(float(item.get("cost")), item.get("impressions")) * 1000),
                # "raw_impressions": (divbyz(item.get("impressions"), float(item.get("raw"))) * 100),
            })

        data = {
            "meta": {
                "group": group,
                "campaign": campaign,
                "media": media,
                "date_from": format_ts(start),
                "date_to": format_ts(end),
                "hourly": hourly
            },
            "objects": q
        }

        return jsonify(**data)

    @Route("/category", methods=["GET"])
    def categories_list(self, app_id=0):
        platform = request.values.get("platform")
        ctype = request.values.get("type")
        obj_list = []

        search = request.values.get("q")

        q = Session.query(Category.name, Category.id)

        if platform:
            q = q.filter(Category.platform == platform)

        if ctype:
            q = q.filter(Category.type == ctype)

        if search and search is not "":
            q = q.filter(Category.name.ilike(u"%{0}%".format(search)))

        q = q.all()

        obj_list += [{"id": z.id, "name": z.name} for z in q]

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/videos", methods=["GET"])
    def videos_list(self):
        account_id = request.environ.get('REMOTE_USER')

        campaign_name_list = {c.id: c.name for c in Session.query(Campaign.id, Campaign.name).filter(Campaign.account == account_id).all()}
        media_list = Session.query(Media).filter(Media.campaign.in_(campaign_name_list.keys())).all()

        objects_list = SAJson.serialize(media_list)
        for item in objects_list:
            item.update({'campaign_name': campaign_name_list.get(item.get('campaign'))})

        grouped_dict = {}
        for item in objects_list:
            if item.get('campaign_name') not in grouped_dict.keys():
                grouped_dict[item.get('campaign_name')] = []

            grouped_dict[item.get('campaign_name')].append(item)

        # pprint(grouped_dict)

        data = {
            'meta': {},
            'objects': [{'name': name, 'media': media} for name, media in grouped_dict.iteritems()]
        }

        return jsonify(**data)
