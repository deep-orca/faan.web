# -*- coding: utf-8 -*-
import json
from pprint import pprint
import traceback
from flask import g, request, redirect, jsonify, abort
import simplejson
from faan.core.X.xflask import Controller, Route, capp, Error
from faan.core.X.mako import render_template
from faan.web.app import account
from faan.web.forms.advertiser import MediaBannerForm, MediaVideoForm, MediaMraidForm, MraidOrderForm
from faan.web.lib.helpers import SAJson
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned, NotOwnedException, NotAllowedException
from faan.core.model.adv.campaign import Campaign
from faan.core.model.meta import Session
from faan.core.model.category import Category
from faan.core.model.adv.media import Media, _Media_States
from faan.core.model.security import Account
from werkzeug.utils import secure_filename
import subprocess
from faan.core.X.xemail import send_email

import os
from shutil import move
from faan.x.util import try_int

from PIL import Image

ALLOWED_IMG_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif',}
ALLOWED_VIDEO_EXTENSIONS = {'mp4',}


def send_contacts_mail(request, account, form):

    subject = u"Заявка на MRAID с Vidiger.com"
    body = u"""<html>
                <head><title>{subject}</title></head>
                <body>
                    <h3>{subject}</h3>
                    <table>
                        <tr><td><strong>Имя</strong></td><td><a href="http://admin.vidiger.com/profile/{id}">{name}</a></td></tr>
                        <tr><td><strong>Email</strong></td><td>{email}</td></tr>
                        <tr><td><strong>Сообщение</strong></td><td>{message}</td></tr>
                    </table>
                </body>
            </html>""".format(subject=subject,
                              name=account.username,
                              email=account.email,
                              id=account.id,
                              message=form.data.get('message'))

    send_email("contact@vidiger.com", subject, body)


@CProtect(IsSigned())
@Controller('/adv/campaign/<int:campaign_id>/media')
class MediaController(object):
    @Error(ex=(NotOwnedException, KeyError, NotAllowedException))
    def error(self, ex):
        return render_template("/error/404_loged.mako" if account else "/error/404.mako"), 404

    @Route("/")
    def index(self, campaign_id):
        #g.campaign = Campaign.GetOr404(id=campaign_id, account=account.id)
        g.campaign = Campaign.Key(campaign_id)

        if g.campaign.account != account.id:
            raise NotOwnedException

        g.medias = Media.All(campaign=g.campaign.id)
        g.media_states = Media.State

        return render_template('/adv/media/list.mako')
        # return render_template('/adv/media/list.tiles.mako')


    @Route('/group_actions', methods=["GET", "POST"])
    def group_actions(self, campaign_id):
        media_list = request.values.get('media_list')
        action = request.values.get('action')
        url = request.values.get('url')

        for m in media_list[:-1].split(','):
            if action == 'delete':
                self.delete(campaign_id, int(m))
            elif action == 'edit_url':
                if url:
                    self.change_url(int(m), url)
            elif action == '1':
                self.activate(campaign_id, int(m))
            else:
                self.deactivate(campaign_id, int(m))

        return jsonify({'error': 0,
                        'action': action,
                        'media_list': media_list[:-1].split(',')})


    @Route("/<int:media_id>", methods=["GET", "POST"])
    @Route("/new", methods=["GET", "POST"])
    def form(self, campaign_id, media_id=None):
        g.campaign = Campaign.Key(campaign_id)
        g.media = Media.Single(id=media_id) or Media.Default()

        # Existing video values
        account_id = request.environ.get('REMOTE_USER')
        campaign_name_list = {c.id: c.name for c in
                              Session.query(Campaign.id, Campaign.name).filter(Campaign.account == account_id).all()}
        media_list = Session.query(Media).filter(Media.campaign.in_(campaign_name_list.keys()), Media.type == Media.Type.VIDEO).all()
        videos = SAJson.serialize(media_list)
        for item in videos:
            item.update({'campaign_name': campaign_name_list.get(item.get('campaign'))})
        grouped_dict = {}
        for item in videos:
            if item.get('campaign_name') not in grouped_dict.keys():
                grouped_dict[item.get('campaign_name')] = []
            grouped_dict[item.get('campaign_name')].append(item)

        g.videos = [{'name': name, 'media': media} for name, media in grouped_dict.iteritems()]
        # End

        form_data = request.form
        try:
            form_type = int(form_data.get('type'))
        except:
            form_type = g.media.type

        banner_form = MediaBannerForm(obj=g.media)
        video_form = MediaVideoForm(obj=g.media)
        mraid_form = MediaMraidForm(obj=g.media)


        # BANNER
        if form_type == Media.Type.BANNER:
            form = MediaBannerForm(request.form, obj=g.media)
            banner_form = form

            if request.method == "POST" and form.validate():
                try:
                    g.media.name                = form.data.get('name')
                    g.media.campaign            = g.campaign.id
                    g.media.type                = Media.Type.BANNER

                    g.media.action_click        = form.data.get('action_click')

                    g.media.daily_limit         = form.data.get('daily_limit')
                    g.media.session_limit       = form.data.get('session_limit')

                    g.media.width = form.width.data
                    g.media.height = form.height.data

                    if (g.media.width, g.media.height) in [Media.BannerSizes.FULLSCREEN_PHONE(),
                                                           Media.BannerSizes.FULLSCREEN_TABLET()]:
                        g.media.placement = Media.Placement.INTERSTITIAL
                    else:
                        g.media.placement = Media.Placement.BANNER

                    Session.add(g.media)
                    Session.flush()

                    media_folder = '%s/%s/%s' % (request.environ['REMOTE_USER'], campaign_id, g.media.id)
                    destination_folder = os.path.join(capp.config['UPLOAD_FOLDER'], media_folder)

                    if not os.path.isdir(destination_folder):
                        os.makedirs(destination_folder)

                    if request.values.get('banner') and request.values.get('banner') != g.media.banner:
                        g.media.state = Media.State.NONE

                        g.media.banner = os.path.join('/global/upload', media_folder, 'banner')

                        temp_banner_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                        *(request.values['banner'].split('/')[-1:]))
                        vdst = os.path.join(destination_folder, 'banner')

                        capp.logger.debug("moving [" + temp_banner_path + "] -> [" + vdst + "]")

                        os.rename(temp_banner_path, vdst)

                    if g.media.action_click == Media.ClickActions.VIDEO:
                        if form.data.get('existing_video'):
                            existing_video              = Media.Key(form.data.get('existing_video'))
                            g.media.video               = existing_video.video
                            g.media.closable            = existing_video.closable
                            g.media.daily_limit         = existing_video.daily_limit
                            g.media.session_limit       = existing_video.session_limit
                            g.media.overlay             = existing_video.overlay
                            g.media.endcard             = existing_video.endcard
                            g.media.uri                 = existing_video.uri
                            g.media.uri_target          = existing_video.uri_target
                            g.media.duration            = existing_video.duration
                        else:
                            if request.values.get('video') and request.values.get('video') != g.media.video:
                                g.media.state = Media.State.NONE

                                temp_video_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                               *(request.values['video'].split('/')[-1:]))

                                vdst = os.path.join(destination_folder, 'source')

                                move(temp_video_path, vdst)

                                # subprocess.Popen(["/usr/local/bin/cnvrt.sh", temp_video_path, vdst], stdout=subprocess.PIPE).wait()

                                r = ""
                                p = subprocess.Popen(["/usr/local/bin/dur.sh", vdst], stdout=subprocess.PIPE)
                                for line in p.stdout:
                                    r += line
                                    p.wait()
                                print p.returncode

                                g.media.duration = int(r.strip())

                                if not g.media.video:
                                    os.symlink(vdst, vdst + ".mp4")

                                g.media.video = os.path.join('/global/upload', media_folder, 'source')
                            if request.values.get('endcard') and request.values.get('endcard') != g.media.endcard:
                                g.media.state = Media.State.NONE

                                g.media.endcard = os.path.join('/global/upload', media_folder, 'endcard')

                                temp_endcard_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                                 *(request.values['endcard'].split('/')[-1:]))
                                vdst = os.path.join(destination_folder, 'endcard')

                                capp.logger.debug("moving [" + temp_endcard_path + "] -> [" + vdst + "]")

                                os.rename(temp_endcard_path, vdst)

                            if request.values.get('overlay') and request.values.get('overlay') != g.media.overlay:
                                g.media.state = Media.State.NONE
                                g.media.overlay = os.path.join('/global/upload', media_folder, 'overlay')

                                temp_overlay_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                                 *(request.values['overlay'].split('/')[-1:]))
                                vdst = os.path.join(destination_folder, 'overlay')

                                capp.logger.debug("moving [" + temp_overlay_path + "] -> [" + vdst + "]")

                                os.rename(temp_overlay_path, vdst)

                            g.media.closable        = form.data.get('closable')
                            g.media.uri_target      = form.data.get('uri_target')
                            g.media.action_end      = form.data.get('action_end')
                    elif g.media.action_click == Media.ClickActions.CALL:
                        g.media.phone_number        = form.data.get('phone_number')
                    elif g.media.action_click == Media.ClickActions.SMS:
                        g.media.phone_number        = form.data.get('phone_number')
                    elif g.media.action_click == Media.ClickActions.URL:
                        g.media.uri                 = form.data.get('uri')
                        g.media.uri_target          = form.data.get('uri_target')
                    elif g.media.action_click == Media.ClickActions.NONE:
                        pass
                    else:
                        capp.logger.exception('Invalid click action type')

                    Session.commit()
                    return redirect("/adv/campaign/%s/media" % g.campaign.id)
                except:
                    capp.logger.exception('EXCEPTION IN /adv/media/form')
                    return redirect("/adv/campaign/%s/media/new" % g.campaign.id)



        # VIDEO
        if form_type == Media.Type.VIDEO:
            form = MediaVideoForm(request.form, obj=g.media)
            video_form = form

            if request.method == "POST" and form.validate():
                try:
                    g.media.name                = form.data.get('name')
                    g.media.campaign            = g.campaign.id
                    g.media.type                = Media.Type.VIDEO

                    g.media.action_click        = form.data.get('action_click')

                    g.media.action_end          = form.data.get('action_end')
                    g.media.closable            = form.data.get('closable')
                    g.media.daily_limit         = form.data.get('daily_limit')
                    g.media.session_limit       = form.data.get('session_limit')

                    g.media.placement           = Media.Placement.INTERSTITIAL

                    Session.add(g.media)
                    Session.flush()

                    media_folder = '%s/%s/%s' % (request.environ['REMOTE_USER'], campaign_id, g.media.id)
                    destination_folder = os.path.join(capp.config['UPLOAD_FOLDER'], media_folder)

                    if not os.path.isdir(destination_folder):
                        os.makedirs(destination_folder)

                    if request.values.get('video') and request.values.get('video') != g.media.video:
                        g.media.state = Media.State.NONE

                        temp_video_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                       *(request.values['video'].split('/')[-1:]))

                        vdst = os.path.join(destination_folder, 'source')

                        move(temp_video_path, vdst)

                        # subprocess.Popen(["/usr/local/bin/cnvrt.sh", temp_video_path, vdst], stdout=subprocess.PIPE).wait()

                        r = ""
                        p = subprocess.Popen(["/usr/local/bin/dur.sh", vdst], stdout=subprocess.PIPE)
                        for line in p.stdout:
                            r += line
                            p.wait()
                        print p.returncode

                        g.media.duration = int(r.strip())

                        if not g.media.video:
                            os.symlink(vdst, vdst + ".mp4")

                        g.media.video = os.path.join('/global/upload', media_folder, 'source')
                    if request.values.get('endcard') and request.values.get('endcard') != g.media.endcard:
                        g.media.state = Media.State.NONE

                        g.media.endcard = os.path.join('/global/upload', media_folder, 'endcard')

                        temp_endcard_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                         *(request.values['endcard'].split('/')[-1:]))
                        vdst = os.path.join(destination_folder, 'endcard')

                        capp.logger.debug("moving [" + temp_endcard_path + "] -> [" + vdst + "]")

                        os.rename(temp_endcard_path, vdst)

                    if request.values.get('overlay') and request.values.get('overlay') != g.media.overlay:
                        g.media.state = Media.State.NONE
                        g.media.overlay = os.path.join('/global/upload', media_folder, 'overlay')

                        temp_overlay_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                         *(request.values['overlay'].split('/')[-1:]))
                        vdst = os.path.join(destination_folder, 'overlay')

                        capp.logger.debug("moving [" + temp_overlay_path + "] -> [" + vdst + "]")

                        os.rename(temp_overlay_path, vdst)

                    if g.media.action_click   == Media.ClickActions.CALL:
                        g.media.phone_number        = form.data.get('phone_number')
                    elif g.media.action_click == Media.ClickActions.SMS:
                        g.media.phone_number        = form.data.get('phone_number')
                    elif g.media.action_click == Media.ClickActions.URL:
                        g.media.uri                 = form.data.get('uri')
                        g.media.uri_target          = form.data.get('uri_target')
                    elif g.media.action_click == Media.ClickActions.NONE:
                        pass
                    else:
                        capp.logger.exception('Invalid click action type')

                    Session.commit()
                    return redirect("/adv/campaign/%s/media" % g.campaign.id)
                except:
                    capp.logger.exception('EXCEPTION IN /adv/media/form')
                    return redirect("/adv/campaign/%s/media/new" % g.campaign.id)



        # MRAID
        if form_type == Media.Type.MRAID:
            form = MediaMraidForm(request.form, obj=g.media)
            mraid_form = form

            if request.method == "POST" and form.validate():
                try:
                    g.media.name = form.name.data
                    g.media.campaign = g.campaign.id
                    g.media.type = Media.Type.MRAID

                    # g.media.banner_size = form.banner_size.data
                    g.media.width = form.width.data
                    g.media.height = form.height.data

                    g.media.daily_limit = form.data.get('daily_limit')
                    g.media.session_limit = form.data.get('session_limit')

                    if (g.media.width, g.media.height) in [Media.BannerSizes.FULLSCREEN_PHONE(), Media.BannerSizes.FULLSCREEN_TABLET()]:
                        g.media.placement = Media.Placement.INTERSTITIAL
                    else:
                        g.media.placement = Media.Placement.BANNER

                    g.media.mraid_type = form.mraid_type.data

                    Session.add(g.media)
                    Session.flush()

                    if g.media.mraid_type == Media.MRAID_Type.FILE:
                        media_folder = '%s/%s/%s' % (request.environ['REMOTE_USER'], campaign_id, g.media.id)
                        destination_folder = os.path.join(capp.config['UPLOAD_FOLDER'], media_folder)

                        g.media.mraid_html = ''
                        g.media.mraid_url = ''

                        if not os.path.isdir(destination_folder):
                            os.makedirs(destination_folder)

                        if request.values.get('mraid_file') and request.values.get('mraid_file') != g.media.mraid_file:
                            g.media.state = Media.State.NONE

                            g.media.mraid_file = os.path.join('/global/upload', media_folder, 'mraid')

                            temp_endcard_path = os.path.join(capp.config['UPLOAD_FOLDER'], 'temp',
                                                             *(request.values['mraid_file'].split('/')[-1:]))
                            vdst = os.path.join(destination_folder, 'mraid')

                            capp.logger.debug("moving [" + temp_endcard_path + "] -> [" + vdst + "]")

                            os.rename(temp_endcard_path, vdst)
                    elif g.media.mraid_type == Media.MRAID_Type.URL:
                        g.media.mraid_url = form.mraid_url.data
                        g.media.mraid_file = ''
                        g.media.mraid_html = ''
                    elif g.media.mraid_type == Media.MRAID_Type.HTML:
                        g.media.mraid_html = form.mraid_html.data
                        g.media.mraid_url = ''
                        g.media.mraid_file = ''
                    else:
                        capp.logger.exception("Invalid MRAID type")

                    Session.commit()
                    return redirect("/adv/campaign/%s/media" % g.campaign.id)
                except:
                    capp.logger.exception('EXCEPTION IN /adv/media/form')
                    return redirect("/adv/campaign/%s/media/new" % g.campaign.id)

        return render_template('/adv/media/form.mako', forms={'banner': banner_form, 'video': video_form, 'mraid': mraid_form}, current_form=form, mraid_order_form=MraidOrderForm())


    @Route('/upload/video', methods=["POST"])
    def uploadVideo(self, campaign_id):
        try:
            f = request.files['video_data']
            fn = secure_filename(f.filename)

            # TODO: extra video checks

            f.save(os.path.join(capp.config['UPLOAD_FOLDER'], "temp", fn))

            return jsonify({'error': 0, 'name': fn})
        except:
            capp.logger.exception("in uploadVideo")
            return jsonify({'error': 1})

    @Route('/upload/endcard', methods=["POST"])
    def uploadEndcard(self, campaign_id):
        try:
            f = request.files['endcard_data']
            fn = secure_filename(f.filename)

            # TODO: extra endcard checks

            f.save(os.path.join(capp.config['UPLOAD_FOLDER'], "temp", fn))

            return jsonify({'error': 0, 'name': fn})
        except:
            capp.logger.exception("in uploadEndcard")
            return jsonify({'error': 1})


    @Route('/upload/banner', methods=["POST"])
    def uploadBanner(self, campaign_id):
        try:
            f = request.files['banner_data']
            filename = secure_filename(f.filename)
            filepath = os.path.join(capp.config['UPLOAD_FOLDER'], "temp", filename)
            f.save(filepath)
            filesize = os.stat(filepath).st_size

            # selected_banner_size = try_int(request.values.get('banner_size'))
            # bw, bh = tuple(int(v) for v in Media.BannerSizes.inverse().get(selected_banner_size).strip('SIZE_').split('x'))
            bw, bh = int(request.values.get('width')), int(request.values.get('height'))

            # PIL's Image performs check of image format
            # If non image file provided - raises exception
            img = Image.open(filepath)
            fw, fh = img.size

            if (fh != bh or fw != bw) and (fw != bh or fh != bw):
                os.remove(filepath)
                raise ValueError('dimensions_mismatch')

            if filesize > 400*1024:
                os.remove(filepath)
                raise ValueError('size_mismatch')

            # TODO: extra banner checks

            return jsonify({'error': 0, 'name': filename})
        except Exception, e:
            capp.logger.exception("in uploadBanner")
            return jsonify({'error': 1, 'msg': e.message})


    @Route('/upload/overlay', methods=["POST"])
    def uploadOverlay(self, campaign_id):
        try:
            f = request.files['overlay_data']
            fn = secure_filename(f.filename)

            # TODO: extra overlay checks

            f.save(os.path.join(capp.config['UPLOAD_FOLDER'], "temp", fn))

            return jsonify({'error': 0, 'name': fn})
        except:
            capp.logger.exception("in uploadOverlay")
            return jsonify({'error': 1})


    @Route('/upload/mraid', methods=["POST"])
    def uploadMraid(self, campaign_id):
        try:
            f = request.files['mraid_data']
            fn = secure_filename(f.filename)

            # TODO: extra overlay checks

            f.save(os.path.join(capp.config['UPLOAD_FOLDER'], "temp", fn))

            return jsonify({'error': 0, 'name': fn})
        except:
            capp.logger.exception("in uploadMraid")
            return jsonify({'error': 1})


    @Route("/<int:media_id>/activate")
    def activate(self, campaign_id, media_id):
        campaign = Campaign.Key(campaign_id)
        media = Media.Key(media_id)

        if campaign.account != account.id:
            raise NotOwnedException

        if media.state == Media.State.NONE or media.state == Media.State.SUSPENDED:
            raise NotAllowedException

        media.state = Media.State.ACTIVE

        try:
            Session.add(media)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("While activating media")

        return redirect("/adv/campaign/%s/media/" % campaign_id)


    @Route("/<int:media_id>/deactivate")
    def deactivate(self, campaign_id, media_id):
        campaign = Campaign.Key(campaign_id)
        media = Media.Key(media_id)

        if campaign.account != account.id:
            raise NotOwnedException

        if media.state == Media.State.NONE or media.state == Media.State.SUSPENDED:
            raise NotAllowedException

        media.state = Media.State.DISABLED

        try:
            Session.add(media)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("While deactivating media")

        return redirect("/adv/campaign/%s/media/" % campaign_id)


    @Route("/<int:media_id>/delete")
    def delete(self, campaign_id, media_id):
        campaign = Campaign.Key(campaign_id)
        media = Media.Key(media_id)

        if campaign.account != account.id:
            raise NotOwnedException

        try:
            Session.delete(media)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("While deactivating media")

        return redirect("/adv/campaign/%s/media/" % campaign_id)

    @Route("/mraid-order", methods=["GET", "POST"])
    def mraid_order_view(self, campaign_id):
        form = MraidOrderForm(request.form)
        if request.method == "POST" and form.validate():
            account = Session.query(Account).filter(Account.id == request.environ['REMOTE_USER']).first()
            send_contacts_mail(request, account, form)
            return jsonify({'status': 'success'})
        return jsonify({'status': 'error', 'errors': form.errors})


    def change_url(self, media_id, url):
        try:
            media = Media.Key(media_id)
            media.uri = url
            Session.flush()
            Session.commit()
            return jsonify({'error': 0})

        except:
            return jsonify({'error': 1})














