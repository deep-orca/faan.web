# coding=utf-8
from flask import request, redirect
from faan.core.X.xemail import send_email

from faan.core.X.xflask import Controller, Route, capp
from faan.core.X.mako import render_template
from faan.core.model.security import Account

from flask.helpers import send_from_directory
from faan.web.forms.contacts import ContactForm


def send_contacts_mail(request, form):
    subject = u"[{path}] Сообщение с Vidiger.com".format(path=request.path.strip('/').capitalize())
    body = u"""<html>
                <head><title>{subject}</title></head>
                <body>
                    <h3>{subject}</h3>
                    <table>
                        <tr><td><strong>Имя</strong></td><td>{name}</td></tr>
                        <tr><td><strong>Email</strong></td><td>{email}</td></tr>
                        <tr><td><strong>Телефон</strong></td><td>{phone}</td></tr>
                        <tr><td><strong>IP</strong></td><td>{ip}</td></tr>
                        <tr><td><strong>Useragent</strong></td><td>{useragent}</td></tr>
                        <tr><td><strong>Сообщение</strong></td><td>{message}</td></tr>
                    </table>
                </body>
            </html>""".format(subject=subject,
                              name=form.data.get('name'),
                              email=form.data.get('email'),
                              phone=form.data.get('phone'),
                              ip=request.remote_addr,
                              useragent=request.user_agent,
                              message=form.data.get('message'),
                              path=request.path.strip('/').capitalize())

    send_email("contact@vidiger.com", subject, body)


@Controller
class IndexController(object):
    @Route("/")
    def index(self, ex=None):
        if request.environ.get('REMOTE_USER'):
            account = Account.Get(request.environ['REMOTE_USER'])
            if account.groups == Account.Groups.ADV:
                return redirect('/adv')
            elif account.groups == Account.Groups.PUB:
                return redirect('/pub')
        return render_template('index/home.mako')

    @Route("/advertisers/")
    def advertisers(self, ex=None):
        return render_template('index/advertisers.mako')

    @Route("/publishers/")
    def publishers(self, ex=None):
        return render_template('index/publishers.mako')

    @Route("/brands/", methods=["GET", "POST"])
    def brands(self, ex=None):
        form = ContactForm(request.form)
        if request.method == "POST" and form.validate():
            # Send email
            send_contacts_mail(request, form)
            return redirect("/brands/?success=true")
        return render_template('index/brands.mako', form=form)

    @Route("/contacts/", methods=["GET", "POST"])
    def contacts(self, ex=None):
        form = ContactForm(request.form)
        if request.method == "POST" and form.validate():
            # Send email
            send_contacts_mail(request, form)
            return redirect("/contacts/?success=true")
        return render_template('index/contacts.mako', form=form)

    @Route("/testad/")
    def testad(self, ex=None):
        return render_template('index/testad.mako')

    @Route("/presentation/")
    def presentation(self, ex=None):
        return render_template('index/presentation.mako')

    @Route("/adformats/demo")
    def formatsdemo(self, ex=None):
        return render_template('index/demo.mako')


