# coding=utf-8
from gettext import gettext
from wtforms import StringField, IntegerField, SelectField, BooleanField, SelectMultipleField, DecimalField
from wtforms.validators import url, input_required, optional, required, data_required, length
from wtforms.widgets import HiddenInput, RadioInput, CheckboxInput, ListWidget
from faan.core.model.adv.media import Media
from faan.core.model.general.sizes import Size
from faan.core.model.pub.application import Application
from faan.core.model.pub.zone import Zone
from faan.web.forms import XForm, XSelectMultipleField


class ApplicationForm(XForm):
    _CONTENT_RATING_CHOICES = [
        (Application.ContentRating.EVERYONE, 0),
        (Application.ContentRating.LOW, 1),
        (Application.ContentRating.MEDIUM, 2),
        (Application.ContentRating.HIGH, 3),
    ]

    _OS_CHOICES = [
        (Application.Os.IOS, "iOS"),
        (Application.Os.ANDROID, "Android"),
        (Application.Os.MOBILE_WEB, "Web")
    ]

    name = StringField(u"Название", validators=[input_required(), length(max=64,
                                                                    message=u'Превышено допустимое количество символов в названии. Максимум 64 знака')], default="")
    store_url = StringField("store_url", validators=[input_required(), url()], default="")
    icon_url = StringField("icon_url", validators=[optional(), url()], default="")
    os = SelectField("os", validators=[input_required()], choices=_OS_CHOICES, coerce=int, default=0)
    content_rating = SelectField("content_rating", validators=[input_required()], choices=_CONTENT_RATING_CHOICES,
                                 coerce=int)
    category_groups = XSelectMultipleField("category", validators=[input_required()], coerce=int)
    # category = XSelectMultipleField("category", validators=[input_required()], coerce=int)
    target_categories = XSelectMultipleField("target_categories", validators=[
        input_required(message=u"Выберите хотя бы одну категорию")], coerce=int)


class ZoneForm(XForm):
    # id = IntegerField("id", validators=[optional()], default=0)
    # application = IntegerField("id", validators=[optional()], default=0)
    # v4vc_cpv = IntegerField("v4vc_cpv", validators=[input_required()])

    # _SIZE_CHOICES = [
    #     (Zone.Size.BANNER, u"320x50"),
    #     (Zone.Size.LARGE_BANNER, u"320x100"),
    #     (Zone.Size.MEDIUM_RECTANGLE, u"300x250"),
    #     (Zone.Size.FULL_BANNER, u"468x60"),
    #     (Zone.Size.LEADERBOARD, u"728x90"),
    # ]

    SIZES = [
        (Zone.Size.BANNER, u'320 &times; 50'),
        (Zone.Size.LARGE_BANNER, u'320 &times; 100'),
        (Zone.Size.MEDIUM_RECTANGLE, u'300 &times; 250'),
        (Zone.Size.FULL_BANNER, u'468 &times; 60'),
        (Zone.Size.LEADERBOARD, u'728 &times; 90'),
        # (Zone.Size.FULLSCREEN_PHONE, u'320 &times; 480 (Полноэкранный)'),
        # (Zone.Size.FULLSCREEN_TABLET, u'1024 &times; 768 (Полноэкранный)'),
    ]

    # General
    type = IntegerField(u"Тип зоны", widget=HiddenInput())
    device_type = IntegerField(u"Тип устройства", widget=HiddenInput())

    name = StringField(u"Название",
                       validators=[
                           data_required(),
                           length(max=64, message=u'Превышено допустимое количество символов в названии. Максимум 64 знака')
                       ])

    session_limit = IntegerField(u"Показов на пользователя за сессию",
                                 validators=[optional()],
                                 default=0,
                                 description=u"Сколько раз реклама может быть показана пользователю за один запуск приложения. <br>0 - без ограничений.")
    daily_limit = IntegerField(u"Показов на пользователя за сутки",
                               validators=[optional()],
                               default=0,
                               description=u"Сколько раз реклама может быть показана пользователю за сутки. <br>0 - без ограничений.")
    mod_factor = IntegerField(u"Кратность показов", validators=[optional()], default=0,
                              description=u"0, 1 - реклама показывается каждый вызов, 2 - каждый второй вызов, и т.д.")

    # Banner
    width = IntegerField(validators=[input_required()], widget=HiddenInput())
    height = IntegerField(validators=[input_required()], widget=HiddenInput())

    refresh_rate = IntegerField(u"Период обновления", description=u"Время в секундах, через которое в зоне появится новый баннер <br> 0 - не обновляется")

    # Interstitial
    show_video = BooleanField(u"Показывать видео")
    v4vc_cpv = IntegerField(u"V4VC стоимость")

    # If +video
    min_duration = IntegerField("min_duration",
                                validators=[input_required()],
                                widget=HiddenInput())
    max_duration = IntegerField("max_duration",
                                validators=[input_required()],
                                widget=HiddenInput())
    closable = IntegerField(u"Время до закрытия",
                            validators=[input_required()],
                            description=u"Время, после которого у пользователя появится возможность пропустить видео",
                            widget=HiddenInput())


class UnitForm(XForm):
    SIZES = [
        (Zone.Size.BANNER, u'320 &times; 50'),
        (Zone.Size.LARGE_BANNER, u'320 &times; 100'),
        (Zone.Size.MEDIUM_RECTANGLE, u'300 &times; 250'),
        (Zone.Size.FULL_BANNER, u'468 &times; 60'),
        (Zone.Size.LEADERBOARD, u'728 &times; 90'),
        (Zone.Size.CUSTOM, u'Вручную'),
    ]

    # General
    type = IntegerField(u"Тип зоны", widget=HiddenInput())
    device_type = IntegerField(u"Тип устройства", widget=HiddenInput())

    name = StringField(u"Название",
                       validators=[
                           data_required(),
                           length(max=64,
                                  message=u'Превышено допустимое количество символов в названии. Максимум 64 знака')
                       ])

    session_limit = IntegerField(u"Показов на пользователя за сессию",
                                 validators=[optional()],
                                 default=0,
                                 description=u"Сколько раз реклама может быть показана пользователю за один запуск приложения. <br>0 - без ограничений.")
    daily_limit = IntegerField(u"Показов на пользователя за сутки",
                               validators=[optional()],
                               default=0,
                               description=u"Сколько раз реклама может быть показана пользователю за сутки. <br>0 - без ограничений.")
    mod_factor = IntegerField(u"Кратность показов", validators=[optional()], default=0,
                              description=u"0, 1 - реклама показывается каждый вызов, 2 - каждый второй вызов, и т.д.")

    # Banner
    width = IntegerField(u"Ширина", validators=[input_required()])#, widget=HiddenInput())
    height = IntegerField(u"Высота", validators=[input_required()])#, widget=HiddenInput())

    refresh_rate = IntegerField(u"Период обновления",
                                description=u"Время в секундах, через которое в зоне появится новый баннер <br> 0 - не обновляется")


# Source base forms
class SourceBaseForm(XForm):
    name = StringField(u"Название", validators=[input_required(), length(max=64, message=u'Превышено допустимое количество символов в названии. Максимум 64 знака')])
    ecpm = DecimalField(u"ECPM", validators=[input_required()])


class SourceBaseVidigerForm(SourceBaseForm):
    """
    Vidiger base form class
    """
    _MEDIA_TYPES = (
        (Media.Type.BANNER, u"Баннер"),
        (Media.Type.VIDEO, u"Видео"),
        (Media.Type.MRAID, u"MRAID"),
    )
    media_types = SelectMultipleField(u"Типы креативов", validators=[input_required()], choices=_MEDIA_TYPES, widget=ListWidget(prefix_label=False), option_widget=CheckboxInput(), coerce=int)


class SourceBaseAdMobForm(SourceBaseForm):
    """
    AdMob base form class
    """
    admob_unit = StringField(u"AdMob unit", validators=[input_required()])


# Vidiger source forms
class SourceInterstitialVidigerForm(SourceBaseVidigerForm):
    """
    Vidiger interstitial form class
    """
    template = '/pub/source/partials/interstitial.mako'

    min_duration = IntegerField(u"Показывать ролики длиной", validators=[input_required()], widget=HiddenInput(), description=u"Выберите диапазон минимальной и максимальной длительности роликов")
    max_duration = IntegerField(u"Показывать ролики длиной", validators=[input_required()], widget=HiddenInput())
    closable = IntegerField(u"Время до закрытия",
                            validators=[input_required()],
                            description=u"Время, после которого у пользователя появится возможность пропустить видео",
                            widget=HiddenInput())

class SourceBannerVidigerForm(SourceBaseVidigerForm):
    """
    Vidiger banner form class
    """
    template = '/pub/source/partials/banner.mako'


# AdMob source forms
class SourceInterstitialAdMobForm(SourceBaseAdMobForm):
    """
    AdMob interstitial form class
    """
    template = '/pub/source/partials/interstitial.mako'


class SourceBannerAdMobForm(SourceBaseAdMobForm):
    """
    AdMob banner form class
    """
    template = '/pub/source/partials/banner.mako'