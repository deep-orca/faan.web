# -*- coding: utf-8 -*-
import datetime
import time
from flask import request
from sqlalchemy import asc, text
from faan.core.model import Stat
from faan.x.util import TimeStamp


def is_testing():
    return 'vidiger.com' in request.url


def stats_to_flot_data(stats):
    rebills = []
    subs = []
    unsubs = []
    ticks = []
    for stat in stats:
        rebills.append([stat.ts_spawn, int(stat.rebills)])
        subs.append([stat.ts_spawn, int(stat.subs)])
        unsubs.append([stat.ts_spawn, int(stat.unsubs)])
        ticks.append([stat.ts_spawn, datetime.date.fromtimestamp(stat.ts_spawn).strftime('%d')])
    return rebills, subs, unsubs, ticks


def get_stats_flot_data(account, project_id=None):
    # 30days data for chart
    ts = TimeStamp.today_tz() + time.timezone
    stats = (Stat.PreQuery()
             .filter(Stat.ts_spawn >= ts - (30 * 60 * 60 * 24),
                     Stat.ts_spawn <  ts + 60 * 60 * 24 - 1,
                     Stat.account == account))

    if project_id:
        stats = stats.filter(Stat.project == project_id)

    stats = (stats.add_column(Stat.Presets.GROUP_DAILY.label('ts_spawn'))
             .group_by(Stat.Presets.GROUP_DAILY)
             .order_by(asc(text('ts_spawn'))).all())

    return stats_to_flot_data(stats) + (stats, )
