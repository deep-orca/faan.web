# -*- encoding: utf-8 -*-

import time
from datetime import date
from flask import g, request, redirect, session

from faan.core.X.xflask import Controller, Route, Error, capp, getCurrentIP
from faan.core.X.mako import render_template

from faan.core.model.security import Account
from faan.core.model.security.account_extra import AccountExtra, AccountExtraCommunication


from faan.core.model.meta import Session
from faan.x.util import try_int
from repoze.who.api import get_api
from faan.web.forms.sign import RegisterForm, SignInForm, RestoreForm, SignInFormCaptcha
from faan.core.X.xemail import send_email_registration, send_email_notify_admin


@Controller('/sign')
class SignController(object):
    
    @Route("/in", methods=["GET", "POST"])
    def xin(self, ex=None):
        g.attempts = session.get("SignInAttempts", 1)
        if session.get("SignInAttempts", 0) >= 3:
            form = SignInFormCaptcha(request.form)
        else:
            form = SignInForm(request.form)

        who_api = get_api(request.environ)

        _error_list = {
            -1: {"message": u"Неверный логин/пароль", "class": "alert-danger"},
            0: {"message": u"Ваш аккаунт находится на рассмотрении", "class": "alert-warning"},
            2: {"message": u"Ваш аккаунт заблокирован", "class": "alert-danger"},
            3: {"message": u"Действие вашего аккаунта временно приостановлено", "class": "alert-warning"}
        }
        
        if request.method == "POST" and form.validate():
            authenticated, headers = who_api.login({
                'login': form.data.get("email"),
                'password': Account.PassHash(form.data.get("password"))
            })

            if authenticated:
                account = Session().query(Account.groups, Account.state)\
                    .filter(Account.id == authenticated.get("repoze.who.userid"))\
                    .all()[0]

                if account.state == Account.State.ACTIVE:
                    if account.groups == Account.Groups.ADV:
                        resp = redirect("/adv")
                    else:
                        resp = redirect("/pub")
                    resp.headers.extend(headers)
                    session["SignInAttempts"] = 1
                    return resp
                else:
                    error = _error_list.get(account.state)
                    form.errors.update({"auth": error})
            else:
                form.errors.update({"auth": _error_list.get(-1)})

            if len(form.errors):
                attempts = session.get("SignInAttempts", 1)
                attempts += 1
                session["SignInAttempts"] = attempts
            
        return render_template('sign/in.mako', form=form)

    @Route("/out", methods=["GET", "POST"])
    def xout(self, ex=None):
        who_api = get_api(request.environ)
        hs = who_api.logout()
        
        resp = redirect("/")
        resp.headers.extend(hs)
        return resp
        
    @Route("/register", methods=["GET", "POST"])
    def register(self):
        form = RegisterForm(request.form)
        if request.method == "POST" and form.validate():
            account = Account.Default()
            account.username = form.data.get("email")
            account.password = 'password'
            # account.password = Account.PassHash(Account.random_password()) # Uncomment when needed
            account.email = form.data.get("email")
            account.type = Account.Type.PARTNER
            account.state = Account.State.NEW
            account.date_registration = date.fromtimestamp(time.time())
            account.balance = 0
            account.groups = form.data.get("groups")

            try:
                Session.add(account)
                Session.flush()
            except:
                capp.logger.exception('EXCEPTION IN /sign/register can\'t add new User')
                return redirect('/sign/register')

            account_extra = AccountExtra.Default()
            account_extra.account_id = account.id
            account_extra.name = form.data.get("fullname")
            account_extra.company = form.data.get("company")
            account_extra.description = form.data.get("description")

            Session.add(account_extra)
            Session.flush()

            if form.data.get("icq"):
                icq = AccountExtraCommunication.Default()
                icq.account_extra_id = account_extra.id
                icq.type = AccountExtraCommunication.Account_communication_types.ICQ
                icq.value = form.data.get("icq")
                Session.add(icq)
            if form.data.get("skype"):
                skype = AccountExtraCommunication.Default()
                skype.account_extra_id = account_extra.id
                skype.type = AccountExtraCommunication.Account_communication_types.SKYPE
                skype.value = form.data.get("skype")
                Session.add(skype)
            if form.data.get("phone"):
                phone = AccountExtraCommunication.Default()
                phone.account_extra_id = account_extra.id
                phone.type = AccountExtraCommunication.Account_communication_types.PHONE
                phone.value = form.data.get("phone")
                Session.add(phone)

            try:
                Session.flush()
                Session.commit()
                send_email_registration(account.email)
                send_email_notify_admin(account.email, account.id)
                return render_template('sign/success.mako')
            except:
                capp.logger.exception('EXCEPTION IN /sign/register CAN\'T REGISTER USER')
                return redirect("/sign/register")

        return render_template('sign/register.mako', form=form)

    @Route("/restore", methods=["GET", "POST"])
    def restore(self, ex=None):
        form = RestoreForm(request.form)
        who_api = get_api(request.environ)

        if request.method == "POST" and form.validate():
            # TODO: Restore password logic
            pass

        return render_template('sign/restore.mako', form=form)