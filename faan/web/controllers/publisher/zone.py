# -*- coding: utf-8 -*-
from pprint import pprint
import traceback
from flask import g, request, redirect, jsonify, abort
from faan.core.X.xflask import Controller, Route, capp, Error
from faan.core.X.mako import render_template
from faan.core.model.meta import Session
from faan.web.app import account
from faan.web.forms.publisher import ZoneForm
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned, NotOwnedException, NotAllowedException
from faan.x.util import try_int

from faan.core.model.pub.zone import Zone
from faan.core.model.pub.application import Application
from faan.core.model.category import Category


@CProtect(IsSigned())
@Controller('/pub/application/<int:app_id>/zone')
class ZoneController():
    @Error(ex=(NotOwnedException, KeyError, NotAllowedException))
    def error(self, ex):
        return render_template("/error/404_loged.mako" if account else "/error/404.mako"), 404

    @Route("/")
    def index(self, app_id):
        g.application = Application.Key(app_id)

        if g.application.account != account.id:
            raise NotOwnedException

        g.zones = Zone.All(application=g.application.id)

        return render_template('/pub/zone/list.mako')


    @Route('/new', methods=['GET', 'POST'])
    @Route('/<int:zone_id>', methods=["GET", "POST"])
    def form(self, app_id, zone_id=None):
        g.app = Application.Key(app_id)
        g.zone = zone = Zone.Single(id=zone_id) or Zone.Default()

        form = ZoneForm(request.form, obj=zone)

        if request.method == 'POST' and form.validate():
            zone.application = g.app.id
            zone.name = form.name.data
            zone.device_type = form.device_type.data

            zone.session_limit = form.session_limit.data
            zone.daily_limit = form.daily_limit.data

            zone.mod_factor = form.mod_factor.data

            if form.type.data == Zone.Type.INTERSTITIAL:
                zone.type = form.type.data
                zone.refresh_rate = 0
                if zone.device_type == Zone.DeviceType.PHONE:
                    zone.width, zone.height = Zone.Size.FULLSCREEN_PHONE()
                elif zone.device_type == Zone.DeviceType.TABLET:
                    zone.width, zone.height = Zone.Size.FULLSCREEN_TABLET()
                zone.show_video = int(form.show_video.data)
                zone.v4vc_cpv = form.v4vc_cpv.data
                if form.show_video.data:
                    zone.closable = form.closable.data
                    zone.min_duration = form.min_duration.data
                    zone.max_duration = form.max_duration.data
                else:
                    zone.closable = 0
                    zone.min_duration = 0
                    zone.max_duration = 0
            elif form.type.data == Zone.Type.BANNER:
                zone.type = form.type.data
                zone.refresh_rate = form.refresh_rate.data
                zone.width = form.width.data
                zone.height = form.height.data
                zone.v4vc_cpv = 0
                zone.show_video = 0
                zone.closable = 0
                zone.min_duration = 0
                zone.max_duration = 0

            zone.flags = zone.flags or Zone.Flag.NONE
            # zone.type = zone.type or Zone.Type.NONE
            zone.state = zone.state or Zone.State.ACTIVE
            try:
                Session.add(zone)
                Session.flush()
                Session.commit()

                print '/pub/application/{0}/zone'.format(g.app.id)

                return redirect('/pub/application/{0}/zone'.format(g.app.id))
            except:
                capp.logger.exception('EXCEPTION IN /app/zone/form')

        return render_template('/pub/zone/form.mako', form=form)

    @Route("/<int:oid>/state/<int:state>")
    def state(self, app_id, oid, state):
        app = Application.Key(app_id)
        obj = Zone.Key(oid)

        if oid and obj.application != app.id and app.account != account.id:
            raise NotOwnedException

        if state not in [1, 2]:
            raise NotAllowedException

        obj.state = state

        try:
            Session.add(obj)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("While setting zone state")

        return redirect("/pub/application/{0}/zone/".format(app_id))

    @Route("/<int:oid>/delete")
    def delete(self, app_id, oid):
        app = Application.Key(app_id)
        obj = Zone.Key(oid)

        if oid and obj.application != app.id and app.account != account.id:
            raise NotOwnedException

        try:
            Session.delete(obj)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("While deleting zone")

        return redirect("/pub/application/{0}/zone/".format(app_id))