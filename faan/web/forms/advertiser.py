# coding=utf-8
from gettext import gettext
from flask import current_app
from wtforms import StringField, IntegerField, SelectField, DateField, DecimalField, SelectMultipleField, FileField, \
    ValidationError, RadioField, TextAreaField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import url, input_required, optional, required, length
from wtforms.widgets import HiddenInput, ListWidget, RadioInput
from faan.core.model import Category
from faan.core.model.adv.campaign import Campaign
from faan.core.model.adv.media import Media
from faan.core.model.meta import Session
from faan.core.model.pub.application import Application
from faan.web.forms import XForm, XSelectMultipleField, XSelectField, DateTSField, SimpleTagField
from faan.web.forms.widgets import BSButtonGroupWidget
from faan.web.lib.helpers import SAJson


class CampaignForm(XForm):
    _OS_CHOICES = [
        (Application.Os.IOS, "iOS"),
        (Application.Os.ANDROID, "Android")
    ]

    _DEVCLASS_CHOICES = [
        (Campaign.DevClass.PHONE, "Phone"),
        (Campaign.DevClass.TABLET, "Tablet")
    ]

    _CONTENT_RATING_CHOICES = [
        (Application.ContentRating.EVERYONE, "Everyone"),
        (Application.ContentRating.LOW, "Low"),
        (Application.ContentRating.MEDIUM, "Medium"),
        (Application.ContentRating.HIGH, "High")
    ]

    name                        = StringField(u"Название", validators=[input_required(), length(max=64,
                                                                    message=u'Превышено допустимое количество символов в названии. Максимум 64 знака')], default="")
    category                    = XSelectField(u"Категория", validators=[input_required()], coerce=int)
    targeting                   = XSelectMultipleField("targeting", validators=[optional()], coerce=int, default=0)
    target_categories_groups    = XSelectMultipleField("target_categories_groups", coerce=int)
    target_platforms            = SelectMultipleField("target_platforms", choices=_OS_CHOICES,
                                           coerce=int)
    target_devclasses           = SelectMultipleField("target_devclasses",
                                            choices=_DEVCLASS_CHOICES, coerce=int)
    target_content_rating       = SelectMultipleField("target_content_rating", validators=[optional()],
                                                choices=_CONTENT_RATING_CHOICES, coerce=int)
    target_geo                  = XSelectMultipleField("target_geo", coerce=long)
    bid                         = DecimalField("bid", validators=[input_required()])
    ts_start                    = DateTSField("ts_start", format="%d.%m.%Y", validators=[optional()])
    ts_end                      = DateTSField("ts_end", format="%d.%m.%Y", validators=[optional()])
    limit_cost_total            = DecimalField("limit_total", validators=[optional()])
    limit_cost_daily            = DecimalField("limit_daily", validators=[optional()])

    target_applications         = SimpleTagField(u"Показывать в этих площадках", coerce=int)

    _PAYMENT_TYPE_CHOICES = [
        (Campaign.PaymentType.CPM,  u'CPM'),
        (Campaign.PaymentType.CPC,  u'CPC'),
        (Campaign.PaymentType.CPA,  u'CPA'),
    ]
    payment_type                = RadioField(u'Тип', validators=[input_required()], choices=_PAYMENT_TYPE_CHOICES, coerce=int,
                                             widget=BSButtonGroupWidget(disabled_options=[3]))

    _GENDER_CHOICES = [
        (Campaign.TargetingGender.MALE,   u'Мужской'),
        (Campaign.TargetingGender.FEMALE, u'Женский'),
    ]
    target_gender               = SelectMultipleField("gender", validators=[optional()], choices=_GENDER_CHOICES, coerce=int)

    _AGE_CHOICES = [
        (Campaign.TargetingAge.AGE_0_12, u"0 - 12"),
        (Campaign.TargetingAge.AGE_12_18, u"12 - 18"),
        (Campaign.TargetingAge.AGE_18_24, u"18 - 24"),
        (Campaign.TargetingAge.AGE_24_32, u"24 - 32"),
        (Campaign.TargetingAge.AGE_32_40, u"32 - 40"),
        (Campaign.TargetingAge.AGE_40_55, u"40 - 55"),
        (Campaign.TargetingAge.AGE_55, u"55+"),
    ]
    target_age                  = SelectMultipleField("age", validators=[optional()], choices=_AGE_CHOICES, coerce=int)


    def validate_target_devclasses(self, field):
        if Campaign.Targeting.DEVCLASS & reduce(lambda r,d: r | d, self.targeting.data, 0):
            if not field.data:
                raise ValidationError(u"Выберите хотя бы один пункт")

    def validate_target_platforms(self, field):
        if Campaign.Targeting.PLATFORM & reduce(lambda r, d: r | d, self.targeting.data, 0):
            if not field.data:
                raise ValidationError(u"Выберите хотя бы один пункт")

    def validate_target_geo(self, field):
        if Campaign.Targeting.GEO & reduce(lambda r, d: r | d, self.targeting.data, 0):
            if not field.data:
                raise ValidationError(u"Выберите хотя бы один пункт")

    def validate_target_categories(self, field):
        if Campaign.Targeting.CATEGORY & reduce(lambda r, d: r | d, self.targeting.data, 0):
            if not field.data:
                raise ValidationError(u"Выберите хотя бы один пункт")

    def validate_target_content_rating(self, field):
        if Campaign.Targeting.CONTENT_RATING & reduce(lambda r, d: r | d, self.targeting.data, 0):
            if not field.data:
                raise ValidationError(u"Выберите хотя бы один пункт")

    def validate_bid(self, field):
        if field.data <= 0:
            raise ValidationError(u"Ставка должна быть больше нуля")


class MediaBannerForm(XForm):
    SIZES = [
        (Media.BannerSizes.BANNER, u'320 &times; 50'),
        (Media.BannerSizes.LARGE_BANNER, u'320 &times; 100'),
        (Media.BannerSizes.MEDIUM_RECTANGLE, u'300 &times; 250'),
        (Media.BannerSizes.FULL_BANNER, u'468 &times; 60'),
        (Media.BannerSizes.LEADERBOARD, u'728 &times; 90'),
        (Media.BannerSizes.FULLSCREEN_PHONE, u'320 &times; 480 (Полноэкранный)'),
        (Media.BannerSizes.FULLSCREEN_TABLET, u'1024 &times; 768 (Полноэкранный)'),
        (Media.BannerSizes.CUSTOM, u'Вручную'),
    ]

    type                = IntegerField(widget=HiddenInput(), validators=[input_required()])
    name                = StringField(u"Название", validators=[input_required(), length(max=64,
                                      message=u'Превышено допустимое количество символов в названии. Максимум 64 знака')])
    uri                 = StringField(u"URL", validators=[optional(), url()])
    uri_target          = IntegerField(u"Открывать во внешнем браузере?", widget=HiddenInput())
    banner              = StringField(u"Баннер", validators=[input_required()], widget=HiddenInput())
    # banner_size         = SelectField(u"Размер", validators=[optional()], choices=_BANNER_SIZE_CHOICES, coerce=tuple)
    width = IntegerField(validators=[input_required()])#, widget=HiddenInput())
    height = IntegerField(validators=[input_required()])#, widget=HiddenInput())

    _ACTIONS_CLICK_CHOICES = [
        (Media.ClickActions.NONE, u"Ничего"),
        (Media.ClickActions.URL, u"Переход по URL"),
        (Media.ClickActions.SMS, u"СМС"),
        (Media.ClickActions.CALL, u"Звонок"),
        (Media.ClickActions.VIDEO, u"Видео"),
    ]
    action_click        = SelectField(u"Действие по клику", choices=_ACTIONS_CLICK_CHOICES, validators=[input_required()], coerce=int)

    video               = StringField(u"Видео", widget=HiddenInput())
    overlay             = StringField(u"Оверлей", widget=HiddenInput())
    endcard             = StringField(u"Лендинг", widget=HiddenInput())
    existing_video      = XSelectField(u"Существующее видео", coerce=int, validators=[optional()])
    closable            = IntegerField(u"Время до закрытия")
    session_limit       = IntegerField(u"Показов на пользователя за сессию")
    daily_limit         = IntegerField(u"Показов на пользователя за сутки")
    action_end          = IntegerField(u"Действие по завершении видео", widget=HiddenInput())
    phone_number        = StringField(u"Номер телефона")

    def validate_banner(self, field):
        if self.banner.object_data == '' and self.banner.data == '':
            raise ValidationError(u"This field is required")
        if not self.banner.data.startswith('/global/upload'):
            raise ValidationError(u"Invalid value")

    def validate_video(self, field):
        if self.action_click.data == 4 and self.video.data == '':
            raise ValidationError(u"This field is required")
        if not self.video.data.startswith('/global/upload') and self.video.data != '':
            raise ValidationError(u"Invalid value")

    def validate_overlay(self, field):
        if not self.overlay.data.startswith('/global/upload') and self.overlay.data != '':
            raise ValidationError(u"Invalid value")

    def validate_endcard(self, field):
        if self.action_end.data == 1 and self.endcard.data == '':
            raise ValidationError(u"This field is required")
        if not self.endcard.data.startswith('/global/upload') and self.endcard.data != '':
            raise ValidationError(u"Invalid value")

    def validate_phone_number(self, field):
        if (self.action_click.data == 1 or self.action_click.data == 2) and self.phone_number.data == '':
            raise ValidationError(u"This field is required")

    def validate_uri(self, field):
        if self.action_click.data == 3 and self.uri.data == '':
            raise ValidationError(u"This field is required")


class MediaVideoForm(XForm):
    type                = IntegerField(widget=HiddenInput(), validators=[input_required()])
    name                = StringField(u"Название", validators=[input_required(), length(max=64,
                                                                         message=u'Превышено допустимое количество символов в названии. Максимум 64 знака')])
    uri                 = StringField(u"URL", validators=[optional(), url()])
    uri_target          = IntegerField(u"Открывать во внешнем браузере?", widget=HiddenInput())
    video               = StringField(u"Видео", validators=[input_required()], widget=HiddenInput())
    overlay             = StringField(u"Оверлей", widget=HiddenInput())
    endcard             = StringField(u"Лендинг", widget=HiddenInput())
    existing_video      = XSelectField(u"Существующее видео", coerce=int)
    closable            = IntegerField(u"Время до закрытия")
    session_limit       = IntegerField(u"Показов на пользователя за сессию")
    daily_limit         = IntegerField(u"Показов на пользователя за сутки")
    action_end          = IntegerField(u"Действие по завершении видео", widget=HiddenInput())

    _ACTIONS_CLICK_CHOICES = [
        (Media.ClickActions.NONE, u"Ничего"),
        (Media.ClickActions.URL, u"Переход по URL"),
        (Media.ClickActions.SMS, u"СМС"),
        (Media.ClickActions.CALL, u"Звонок"),
    ]
    action_click        = SelectField(u"Действие по клику", choices=_ACTIONS_CLICK_CHOICES, validators=[input_required()], coerce=int)

    phone_number        = StringField(u"Номер телефона")

    def validate_video(self, field):
        if self.video.object_data == '' and self.video.data == '':
            raise ValidationError(u"This field is required")
        if not self.video.data.startswith('/global/upload') and self.video.data != '':
            raise ValidationError(u"Invalid value")

    def validate_overlay(self, field):
        if not self.overlay.data.startswith('/global/upload') and self.overlay.data != '':
            raise ValidationError(u"Invalid value")

    def validate_endcard(self, field):
        if self.action_end.data == 1 and self.endcard.data == '':
            raise ValidationError(u"This field is required")
        if not self.endcard.data.startswith('/global/upload') and self.endcard.data != '':
            raise ValidationError(u"Invalid value")

    def validate_phone_number(self, field):
        if (self.action_click.data == 1 or self.action_click.data == 2) and self.phone_number.data == '':
            raise ValidationError(u"This field is required")

    def validate_uri(self, field):
        if self.action_click.data == 3 and self.uri.data == '':
            raise ValidationError(u"This field is required")


class MediaMraidForm(XForm):
    SIZES = [
        (Media.BannerSizes.BANNER, u'320 &times; 50'),
        (Media.BannerSizes.LARGE_BANNER, u'320 &times; 100'),
        (Media.BannerSizes.MEDIUM_RECTANGLE, u'300 &times; 250'),
        (Media.BannerSizes.FULL_BANNER, u'468 &times; 60'),
        (Media.BannerSizes.LEADERBOARD, u'728 &times; 90'),
        (Media.BannerSizes.FULLSCREEN_PHONE, u'320 &times; 480 (Полноэкранный)'),
        (Media.BannerSizes.FULLSCREEN_TABLET, u'1024 &times; 768 (Полноэкранный)'),
        (Media.BannerSizes.CUSTOM, u'Вручную'),
    ]

    type                = IntegerField(widget=HiddenInput(), validators=[input_required()])
    name                = StringField(u"Название", validators=[input_required(), length(max=64,
                                                                         message=u'Превышено допустимое количество символов в названии. Максимум 64 знака')])

    mraid_type          = IntegerField(u"Тип", widget=HiddenInput())
    mraid_file          = StringField(u"MRAID ZIP", widget=HiddenInput(), description=u"ZIP-архив с MRAID")
    mraid_url           = StringField(u"MRAID URL", description=u"MRAID URL", validators=[optional(), url()])
    mraid_html          = TextAreaField(u"MRAID HTML", description=u"MRAID HTML")

    session_limit = IntegerField(u"Показов на пользователя за сессию")
    daily_limit = IntegerField(u"Показов на пользователя за сутки")

    # banner_size         = SelectField(u"Размер", validators=[optional], choices=_BANNER_SIZE_CHOICES, coerce=tuple)
    width               = IntegerField(validators=[input_required()])#, widget=HiddenInput())
    height              = IntegerField(validators=[input_required()])#, widget=HiddenInput())

    def validate_mraid_file(self, field):
        if not self.mraid_file.data.startswith('/global/upload') and self.mraid_file.data != '':
            raise ValidationError(u"Invalid value")


class MraidOrderForm(XForm):
    message = TextAreaField(u"Описание", validators=[optional()])