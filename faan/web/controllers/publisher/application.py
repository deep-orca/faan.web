# -*- coding: utf-8 -*-
from itertools import chain
from pprint import pprint
from flask import g, request, redirect, jsonify, abort
from sqlalchemy.orm.sync import populate
from faan.core.X.xflask import Controller, Route, capp, Error
from faan.core.X.mako import render_template
from faan.core.model.meta import Session
from faan.web.app import account
from faan.web.forms.publisher import ApplicationForm
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned, NotOwnedException, NotAllowedException
from faan.x.util import try_int

from faan.core.model.pub.application import Application
from faan.core.model.category import Category
from faan.core.model.pub.zone import Zone
from __builtin__ import map
from sqlalchemy import func as F

import traceback

@CProtect(IsSigned())
@Controller('/pub/application')
class AppsController():
    @Error(ex=(NotOwnedException, KeyError, NotAllowedException))
    def error(self, ex):
        return render_template("/error/404_loged.mako" if account else "/error/404.mako"), 404

    @Route("/")
    def index(self):
        """
        App list controller
        """
        g.applications = Application.All(account=account.id)

        q = Session.query(F.count(Zone.id).label("count"), Zone.application, Zone.state)\
            .filter(Zone.application.in_([x.id for x in g.applications]))\
            .group_by(Zone.state, Zone.application).all()

        _zstates = {
            0: "new",
            1: "active",
            2: "inactive",
        }

        _azones = {}
        for z in q:
            if not _azones.get(z.application):
                _azones[z.application] = {}
            _azones[z.application].update({_zstates.get(z.state): z.count})

        for a in g.applications:
            a.zones = _azones.get(a.id, {})

        g.categories_dict = dict([(c.id, c.name) for c in Category.All()])
        return render_template('/pub/application/list.mako')

    @Route('/<int:app_id>', methods=['GET', 'POST'])
    @Route('/new', methods=["GET", "POST"])
    def form(self, app_id=None):

        g.application = application = Application.Single(id=app_id) or Application.Default()

        if app_id and application.account != account.id:
            raise NotOwnedException

        is_new = True if not application.id else False

        form = ApplicationForm(request.form, obj=application)

        pprint(form.data)

        g.zone = Zone.All(application=application.id)

        if request.method == 'POST' and form.validate():
            #print form.data
            application.os = try_int(form.data.get('os'), Application.Os.NONE)
            application.name = form.data.get('name')

            application.category_groups = form.data.get('category_groups')

            if application.os == Application.Os.MOBILE_WEB:
                categories = Session.query(Category).filter(Category.type == 3, Category.id.in_(form.category_groups.data)).all()
                application.category = list(set(chain.from_iterable([c.related_categories for c in categories])))
            else:
                application.category = form.category_groups.data

            application.content_rating = form.data.get('content_rating')
            application.store_url = form.data.get('store_url')
            application.icon_url = form.data.get('icon_url')
            application.target_categories = form.data.get('target_categories')
            application.flags = application.flags or Application.Flag.NONE
            application.type = application.type or Application.Type.REGULAR
            application.state = application.state or Application.State.NONE
            application.account = account.id

            try:
                Session.add(application)
                Session.flush()
                Session.commit()
                if not is_new:
                    return redirect('/pub/application/')
                else:
                    return redirect('/pub/application/{0}/unit/new'.format(application.id))
            except:
                capp.logger.exception("While updating or creating app")

        #g.categories = Category.All()

        return render_template('/pub/application/form.mako', form=form)

    @Route("/<int:app_id>/state/<int:state>")
    def state(self, app_id, state):
        app = Application.Key(app_id)

        if app_id and app.account != account.id:
            raise NotOwnedException

        if app.state == 0 and state not in [1, 2]:
            raise NotAllowedException

        app.state = state

        try:
            Session.add(app)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("While changing app state")

        return redirect("/pub/application/")

    @Route("/<int:app_id>/delete")
    def delete(self, app_id):
        app = Application.Key(app_id)

        if app_id and app.account != account.id:
            raise NotOwnedException

        try:
            Session.delete(app)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("While deleting application")

        return redirect("/pub/application/")