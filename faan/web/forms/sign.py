# -*- coding: utf-8 -*-
from flask.ext.wtf import RecaptchaField
from . import XForm, XEmail
from .widgets import RecaptchaWidget
# from .widgets import XRecaptchaWidget
from wtforms import validators, StringField, IntegerField, TextAreaField


RECAPTCHA_HTML = u'''
<div id="recaptcha_widget" class="form-recaptcha">
<div class="form-recaptcha-img" style="width: 312px">
<a id="recaptcha_image" href="#" class="text-center"></a>
        <div class="recaptcha_only_if_incorrect_sol" style="color:red">
             Попробуйте еще раз
        </div>
    </div>
    <div class="input-group" style="width: 312px">
        <input type="text" class="form-control" id="recaptcha_response_field" name="recaptcha_response_field">
        <div class="input-group-btn">
            <a class="btn btn-default" href="javascript:Recaptcha.reload()"><i class="fa fa-refresh"></i></a>
            <a class="btn btn-default recaptcha_only_if_image" href="javascript:Recaptcha.switch_type('audio')"><i title="Get an audio CAPTCHA" class="fa fa-headphones"></i></a>
            <a class="btn btn-default recaptcha_only_if_audio" href="javascript:Recaptcha.switch_type('image')"><i title="Get an image CAPTCHA" class="fa fa-picture-o"></i></a>
            <a class="btn btn-default" href="javascript:Recaptcha.showhelp()"><i class="fa fa-question"></i></a>
        </div>
    </div>
    <p class="help-block">
        <span class="recaptcha_only_if_image">
             Введите слова, указанные выше
        </span>
        <span class="recaptcha_only_if_audio">
             Введите числа, которые слышите
        </span>
    </p>
</div>
<script type="text/javascript">var RecaptchaOptions = %(options)s;</script>
<script type="text/javascript" src="%(script_url)s"></script>
<noscript>
  <div><iframe src="%(frame_url)s" height="300" width="500"></iframe></div>
  <div>
    <textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
    <input type="hidden" name="recaptcha_response_field"
           value="manual_challenge" />
  </div>
</noscript>
'''


class RegisterForm(XForm):
    email = StringField("email", validators=[XEmail(), validators.DataRequired(message=u"Это обязательное поле")])
    fullname = StringField("fullname", validators=[validators.DataRequired(message=u"Это обязательное поле")])
    groups = IntegerField("groups", validators=[validators.DataRequired(message=u"Это обязательное поле")])
    company = StringField("company", validators=[])
    phone = StringField("phone", validators=[])
    skype = StringField("skype", validators=[])
    icq = StringField("icq", validators=[])
    description = TextAreaField("description", validators=[])
    recaptcha = RecaptchaField(widget=RecaptchaWidget(theme='custom', lang='ru', html=RECAPTCHA_HTML))


class SignInForm(XForm):
    email = StringField("email", validators=[validators.Email(), validators.DataRequired(message=u"Это обязательное поле")])
    password = StringField("password", validators=[validators.DataRequired(message=u"Это обязательное поле")])


class SignInFormCaptcha(XForm):
    email = StringField("email",
                        validators=[validators.Email(), validators.DataRequired(message=u"Это обязательное поле")])
    password = StringField("password", validators=[validators.DataRequired(message=u"Это обязательное поле")])
    recaptcha = RecaptchaField(widget=RecaptchaWidget(theme='custom', lang='ru', html=RECAPTCHA_HTML))


class RestoreForm(XForm):
    email = StringField("email", validators=[validators.Email(), validators.DataRequired(message=u"Это обязательное поле")])