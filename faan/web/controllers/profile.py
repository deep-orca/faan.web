# -*- coding: utf-8 -*-

from flask import g, request, redirect

from faan.core.model.meta import Session
from faan.core.X.xflask import Controller, Route
from faan.core.X.mako import render_template

from faan.x.security import CProtect
from faan.web.lib.security import IsSigned
from faan.core.model.security.account import  Account
from faan.core.model.security.account_extra import AccountExtra, \
    AccountExtraCommunication, AccountPurchasingInfo
from faan.core.model.adv.statistics import AdvStat
from faan.core.model.pub.statistics import PubStat
from faan.core.model.security.message import Message
import time
from datetime import datetime, timedelta, date

@CProtect(IsSigned())
@Controller("/adv/profile")
class ProfileController():

    @Route("/", methods=["GET"])
    def index(self):
        g.account = Account.Get(request.environ['REMOTE_USER'])
        try:
            g.extra = AccountExtra.One(account_id=g.account.id)
        except:
             g.extra = AccountExtra.Default()
             g.extra.account_id = g.account.id
             Session.add(g.extra)
             Session.flush()
             Session.commit()
        q = Session.query(AccountExtraCommunication)\
            .filter(AccountExtraCommunication.account_extra_id == g.extra.id)\
            .order_by(AccountExtraCommunication.type)
        communications = q.all()
        g.communications = {k: '' for k in range(1, 9)}
        for c in communications:
            g.communications[c.type] = c.value
        try:
            q = Session.query(AccountPurchasingInfo)\
                .filter(AccountPurchasingInfo.account_extra_id == g.extra.id)
            purchases = q.all()
        except AttributeError:
            purchases = []

        g.active_purchase = 0
        g.purchases = {k: '' for k in range(1, 6)}
        for p in purchases:
            if p.state == AccountPurchasingInfo.State.ACTIVE:
                g.active_purchase = p.type
            g.purchases[p.type] = p.value
        q = Session.query(Message)\
            .filter(Message.message_producer_id == g.account.id).\
            order_by(Message.date.desc())

        g.messages = q.all()
        month_before = time.mktime((date.fromtimestamp(time.time()) -
                                     timedelta(days=30)).timetuple())
        g.stats = None
        #if g.account.groups == Account.Groups.ADV:
        #    q = Session.query(AdvStat)\
        #        .filter(AdvStat.account==g.account.id)\
        #        .filter(AdvStat.ts_spawn > month_before)\
        #        .order_by(AdvStat.ts_spawn.desc())
        #    g.stats = q.all()
        #elif g.account.groups == Account.Groups.PUB:
        #    q = Session.query(PubStat)\
        #        .filter(PubStat.account == g.account.id)\
        #        .filter(AdvStat.ts_spawn > month_before)\
        #        .order_by(AdvStat.ts_spawn.desc())
        #    g.stats = q.all()
        ##@TODO: if other accont type!

        return render_template('index/profile.mako')

    @Route("/edit", methods=["GET", "POST"])
    def edit_profile(self):
        post_form = request.values.get('post_form')
        if post_form == 'communication_data':
            return self.edit_communication()
        elif post_form == 'password_data':
            return self.change_password()
        elif post_form == 'personal_data':
            return self.edit_extra()
        elif post_form == 'purchase_data':
            return self.edit_purchase_info()

    def edit_extra(self):
        try:
            extra = AccountExtra.Get(request.values.get('extra'))
            extra.name = request.values.get('name', extra.name)
            extra.surname = request.values.get('surname', extra.surname)
            Session.flush()
            Session.commit()
        except:
            pass
        return redirect(str(request.path).replace('edit', '#tab_2-2'))

    def edit_purchase_info(self):
        extra = AccountExtra.Get(request.values.get('extra'))
        q = Session.query(AccountPurchasingInfo)\
            .filter(AccountPurchasingInfo.account_extra_id == extra.id)
        purchases = q.all()
        pdict = {}
        pkeys = []
        pactive = 0
        pstate = False
        for r in range(1, 6):
            if request.values.get('type_%s' % r) == 'on':
                pactive = r
            if request.values.get('purchase_%s' % r):
                pdict[r] = request.values.get('purchase_%s' % r)

        for p in purchases:
            pkeys.append(p.type)
            if p.type in pdict.keys():
                p.value = pdict.get(p.type)
            if p.type == pactive:
                p.state = AccountPurchasingInfo.State.ACTIVE
            else:
                p.state = AccountPurchasingInfo.State.NONE

        Session.commit()
        for p in list(set(pdict.keys()) - set(pkeys)):
            if p == pactive:
                pstate = True
            self.create_purchase(extra.id, p, pdict[p], pstate)
        return redirect(str(request.path).replace('edit', '#tab_4-4'))

    def create_purchase(self, extra_id, ptype, value, state):
        purchase = AccountPurchasingInfo()
        purchase.account_extra_id = extra_id
        purchase.type = ptype
        purchase.value = value
        if state:
            purchase.state = AccountPurchasingInfo.State.ACTIVE
        else:
            purchase.state = AccountPurchasingInfo.State.NONE
        Session.add(purchase)
        Session.flush()
        Session.commit()


    def edit_communication(self):
        account = Account.Get(request.environ['REMOTE_USER'])
        extra = AccountExtra.One(account_id=account.id)

        q = Session.query(AccountExtraCommunication)\
            .filter(AccountExtraCommunication.account_extra_id == extra.id)\
            .order_by(AccountExtraCommunication.type)
        communications = q.all()
        cdict ={}
        ckeys = []
        for r in range(1, 9):
            if request.values.get('communication_%s' % r):
                cdict[r] = request.values.get('communication_%s' % r)
        for c in communications:
            ckeys.append(c.type)
            if c.type in cdict.keys():
                c.value = cdict.get(c.type)
        Session.flush()
        Session.commit()
        for c in list(set(cdict.keys()) - set(ckeys)):
            self.create_comunnication(c, cdict[c])

        return redirect(str(request.path).replace('edit', '#tab_3-3'))

    def create_comunnication(self, ctype, value):
        com = AccountExtraCommunication()
        com.type = ctype
        com.account_extra_id = request.values.get('extra')
        com.value = value
        Session.add(com)
        Session.flush()
        Session.commit()


    def change_password(self):
        try:
            old = request.values['old']
            pass0 = request.values['pass0']
            pass1 = request.values['pass1']

            account = Account.Get(request.environ['REMOTE_USER'])
            if account.password == old and pass0 == pass1:
                account.password = pass0
                Session.flush()
                Session.commit()
        except:
            pass
        return redirect(str(request.path).replace('edit', '#tab_5-5'))


@CProtect(IsSigned())
@Controller("/pub/profile")
class PubProfile(ProfileController):
    pass
