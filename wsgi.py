import os
from paste.deploy import loadapp

application = loadapp('config:' + os.path.dirname(__file__) + '/configurations/development.ini')

def serve(port=8099):
    try:
        print 'trying gevent'
        from gevent import wsgi                             #@UnresolvedImport
        
        wsgi.WSGIServer(('127.0.0.1', port), application, spawn=None).serve_forever()
    except:
        print 'trying paste'
        from paste.httpserver import serve
        serve(application, 'localhost', port)
        
if __name__ == "__main__":
    serve()


