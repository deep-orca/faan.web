# -*- coding: utf-8 -*-
from decimal import Decimal
from pprint import pprint
import traceback
from flask import g, request, redirect, jsonify, abort
from faan.core.X.xflask import Controller, Route, capp, Error
from faan.core.X.mako import render_template
from faan.core.model.adv.media import Media
from faan.core.model.meta import Session
from faan.web.app import account
from faan.web.forms.publisher import UnitForm
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned, NotOwnedException, NotAllowedException
from faan.x.util import try_int

from faan.core.model.pub.zone import Zone
from faan.core.model.pub.application import Application
from faan.core.model.category import Category
from faan.core.model.pub.unit import Unit

from faan.core.model.pub.source import Source
from faan.core.X.helpers.source import SourceHelper, SourceHelperFormObject
from faan.web.forms.publisher import (
    SourceBannerVidigerForm,
    SourceInterstitialVidigerForm,
    SourceBannerAdMobForm,
    SourceInterstitialAdMobForm
)

source_helper = SourceHelper([
    SourceHelperFormObject(Source.Type.VIDIGER, Unit.Type.BANNER, SourceBannerVidigerForm),
    SourceHelperFormObject(Source.Type.VIDIGER, Unit.Type.INTERSTITIAL, SourceInterstitialVidigerForm),
    SourceHelperFormObject(Source.Type.ADMOB, Unit.Type.BANNER, SourceBannerAdMobForm),
    SourceHelperFormObject(Source.Type.ADMOB, Unit.Type.INTERSTITIAL, SourceInterstitialAdMobForm),
])


@CProtect(IsSigned())
@Controller('/pub/application/<int:app_id>/unit')
class UnitController():
    
    @Error(ex=(NotOwnedException, KeyError, NotAllowedException))
    def error(self, ex):
        return render_template("/error/404_loged.mako" if account else "/error/404.mako"), 404

    @Route("/")
    def index(self, app_id):
        g.application = Application.Key(app_id)

        if g.application.account != account.id:
            raise NotOwnedException

        g.units = Unit.All(application=g.application.id)

        return render_template('/pub/unit/list.mako')


    @Route('/new', methods=['GET', 'POST'])
    @Route('/<int:unit_id>', methods=["GET", "POST"])
    def form(self, app_id, unit_id=None):
        g.app  = Application.Key(app_id)
        g.unit = unit = Unit.Get(unit_id) or Unit.Default()

        form = UnitForm(request.form, obj=unit)

        is_new = True if not unit.id else False

        if request.method == 'POST' and form.validate():
            unit.application = g.app.id
            unit.name = form.name.data

            unit.session_limit = form.session_limit.data
            unit.daily_limit = form.daily_limit.data

            unit.mod_factor = form.mod_factor.data

            unit.device_type = form.device_type.data

            if form.type.data == Unit.Type.INTERSTITIAL:
                unit.type = form.type.data
                unit.refresh_rate = 0
                if not unit.id:
                    if form.device_type.data == Unit.DeviceType.PHONE:
                        unit.width, unit.height = Unit.Size.FULLSCREEN_PHONE()
                    elif form.device_type.data == Unit.DeviceType.TABLET:
                        unit.width, unit.height = Unit.Size.FULLSCREEN_TABLET()
            elif form.type.data == Unit.Type.BANNER:
                unit.type = form.type.data
                unit.refresh_rate = form.refresh_rate.data
                unit.width = form.width.data
                unit.height = form.height.data

            unit.flags = unit.flags or Unit.Flag.NONE
            unit.state = unit.state or Unit.State.ACTIVE
            try:
                Session.flush()
                Session.add(unit)
                Session.commit()

                if is_new:

                    # Creating default Vidiger source
                    SourceCls, FormCls = source_helper.get(Source.Type.VIDIGER, g.app.os, unit.type)

                    source = SourceCls.Default()
                    source.unit = unit.id
                    source.type = Source.Type.VIDIGER
                    source.state = Source.State.ACTIVE
                    source.name = u'Vidiger'
                    source.ecpm = Decimal(1.00)
                    source.media_types = [Media.Type.BANNER, Media.Type.VIDEO, Media.Type.MRAID]

                    Session.flush()
                    Session.add(source)
                    Session.commit()

                    redirect_url = '/pub/application/{0}/unit/{1}/source/{2}?message=default'.format(g.app.id, unit.id, source.id)
                else:
                    redirect_url = '/pub/application/{0}/unit'.format(g.app.id)

                return redirect(redirect_url)
            except Exception, e:
                print e
                capp.logger.exception('EXCEPTION IN /app/unit/form')

        return render_template('/pub/unit/form.mako', form=form)

    @Route("/<int:oid>/<string:state>")
    def state(self, app_id, oid, state):
        app = Application.Key(app_id)
        obj = Unit.Key(oid)

        states = {
            'activate': Unit.State.ACTIVE,
            'deactivate': Unit.State.DISABLED,
        }

        state = states.get(state)

        if oid and obj.application != app.id and app.account != account.id:
            raise NotOwnedException

        if state not in [1, 2]:
            raise NotAllowedException

        obj.state = state

        try:
            Session.flush()
            Session.add(obj)
            Session.commit()
        except Exception, e:
            print e
            capp.logger.exception("While setting unit state")

        return redirect("/pub/application/{0}/unit/".format(app_id))

    @Route("/<int:oid>/delete")
    def delete(self, app_id, oid):
        app = Application.Key(app_id)
        obj = Unit.Key(oid)

        if oid and obj.application != app.id and app.account != account.id:
            raise NotOwnedException

        try:
            Session.delete(obj)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("While deleting unit")

        return redirect("/pub/application/{0}/unit/".format(app_id))
    
    
