# coding=utf-8
import datetime, time
import traceback

from faan.core.model.meta import Session
from faan.core.model.security import Account
from flask_wtf import Form
from wtforms import validators, ValidationError, SelectFieldBase, widgets, SelectMultipleField, SelectField, Field
from wtforms.compat import text_type

#session = Session()


class XForm(Form):
    def __init__(self, *args, **kwargs):
        super(XForm, self).__init__(*args, **kwargs)

    @staticmethod
    def get_session():
        return Session()

    def html(self, *args, **kwargs):
        try:
            fields = [
                field.__call__(
                    form_cls=kwargs.pop("form_cls", "form"),
                    label_width=kwargs.pop("label_width", None),
                    field_width=kwargs.pop("field_width", None),
                    #has_label=kwargs.pop("has_label", True),
                    **(kwargs.get(field.name, {}))
                )
                for field in self
            ][1:]

            fields.append(self.csrf_token.__call__())


            html = "<form id='%(form_id)s' class='%(form_class)s' method='%(method)s'>%(fields)s</form>" % ({
                "method": kwargs.get("method", "POST"),
                "fields": "\n".join(fields),
                "form_class": kwargs.get("form_cls", ""),
                "form_id": kwargs.get("form_id", "form")
            })

            #print html
            return html
        except:
            print traceback.format_exc()


class XEmail(validators.Email):
    def __init__(self, message=None):
        # TODO: Add args for handling different columns, not only `Account.email`
        super(XEmail, self).__init__(message=message)

    def __call__(self, form, field):
        message = self.message
        email = field._value()

        try:
            account = form.get_session().query(Account.email).filter(Account.email == email).all()[0]
        except IndexError:
            account = None

        if account is not None:
            message = field.gettext(u"Пользователь с таким адресом уже зарегистрирован")
            raise ValidationError(message)

        if message is None:
            message = field.gettext(u"Неверный адрес электронной почты")

        super(validators.Email, self).__call__(form, field, message)


class XSelectMultipleField(SelectMultipleField):
    # widget = widgets.Select()

    def __init__(self, label=None, validators=None, coerce=text_type, choices=None, **kwargs):
        super(XSelectMultipleField, self).__init__(label, validators, **kwargs)
        self.coerce = coerce

    def process_formdata(self, valuelist):
        try:
            if len(valuelist) == 1 and valuelist[0] == u'':
                valuelist = []
            if len(valuelist) == 1 and valuelist[0].index(",") >= 0:
                valuelist = valuelist[0].split(",")
        except ValueError:
            pass

        super(XSelectMultipleField, self).process_formdata(valuelist)

    def iter_choices(self):
        return []

    def pre_validate(self, form):
        pass


class XSelectField(SelectField):
    widget = widgets.Select()

    def __init__(self, label=None, validators=None, coerce=text_type, choices=None, **kwargs):
        super(XSelectField, self).__init__(label, validators, **kwargs)
        self.coerce = coerce

    def iter_choices(self):
        return []

    def pre_validate(self, form):
        pass


class DateTimeTSField(Field):
    """
    A text field which stores a `datetime.datetime` matching a format.
    """
    widget = widgets.TextInput()

    def __init__(self, label=None, validators=None, format='%d.%m.%Y %H:%M:%S', **kwargs):
        super(DateTimeTSField, self).__init__(label, validators, **kwargs)
        self.format = format

    def _value(self):
        if self.raw_data:
            return ' '.join(self.raw_data)
        else:
            return self.data and self.data.strftime(self.format) or ''

    def process_formdata(self, valuelist):
        if valuelist:
            date_str = ' '.join(valuelist)
            try:
                self.data = int(datetime.time.mktime(datetime.datetime.strptime(date_str, self.format).timetuple()))
            except ValueError:
                self.data = None
                raise ValueError(self.gettext('Not a valid timestamp value'))
            
            
class DateTSField(DateTimeTSField):
    def __init__(self, label=None, validators=None, format='%d.%m.%Y', **kwargs):
        super(DateTSField, self).__init__(label, validators, **kwargs)
        self.format = format

    def process_formdata(self, valuelist):
        if valuelist:
            date_str = ' '.join(valuelist)
            try:
                self.data = int(time.mktime(datetime.datetime.strptime(date_str, self.format).timetuple()))
            except ValueError:
                self.data = 0
                raise ValueError(self.gettext('Not a valid timestamp value'))


class TagField(XSelectMultipleField):
    widget = widgets.HiddenInput()

    def __init__(self, label=None, validators=None, tag_model_cls=None, **kwargs):
        super(TagField, self).__init__(label, validators, **kwargs)
        if tag_model_cls:
            self.tag_model_cls = tag_model_cls
        else:
            raise ValueError("tag_model_cls is None")

    def _value(self):
        return ",".join([t.name for t in Session.query(self.tag_model_cls).filter(
            self.tag_model_cls.id.in_(self.data)).all()]) if self.data else ""


class SimpleTagField(XSelectMultipleField):
    widget = widgets.HiddenInput()

    def __init__(self, label=None, validators=None, **kwargs):
        super(SimpleTagField, self).__init__(label, validators, **kwargs)

    def _value(self):
        return ",".join([str(s) for s in self.data])