# -*- coding: utf-8 -*-
from flask import g, request
from faan.core.X.xflask import Controller, Route
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned

from faan.core.model.pub.statistics import PubStat
from faan.x.util.timestamp import TimeStamp
import time
from faan.core.model.pub.application import Application
from faan.core.model.pub.zone import Zone

from datetime import datetime


@CProtect(IsSigned())
@Controller('/pub/stat')
class PubStatController():

    @Route("/", methods=["GET", "POST"])
    @Route("/<group>", methods=["GET", "POST"])
    def index(self, group = "date"):
        g.group     = group or "date"
        g.application  = request.values.get('application')
        g.zone     = request.values.get('zone')

        try:
            g.date_period = request.values['date']
            g.ts, g.te = request.values['date'].split(' - ')
            g.ts = int(time.mktime(datetime.strptime(g.ts,
                                                     "%d.%m.%Y").timetuple()))
            g.te = int(time.mktime(datetime.strptime(g.te,
                                                     "%d.%m.%Y").timetuple()))
        except (KeyError, IndexError, ValueError, NameError, TypeError):
            g.ts = TimeStamp.today_tz() + time.timezone
            g.te = TimeStamp.today_tz() + time.timezone

        return render_template('/pub/stat/index.mako')
