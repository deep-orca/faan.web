# coding=utf-8

from faan.core.X.xflask import Controller, Route, capp
from flask.helpers import send_from_directory
from werkzeug.utils import redirect

@Controller
class GlobalStaticController():
    
    @Route("/global/<path:path>")
    def global_serve(self, path):
        try:
            return send_from_directory(capp.config['GLOBAL_FOLDER'], path)
        except:
            return redirect("http://faan.me/global/" + path)


