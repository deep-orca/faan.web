# -*- coding: utf-8 -*-
from decimal import Decimal
from pprint import pprint
import time
import urllib
from urlparse import urlunparse
import datetime
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.util import KeyedTuple

from flask import request

from webhelpers.html import tags
from faan.core.X.helpers.menu import Menu, MenuItem
from faan.core.model.meta import Session
# from faan.web.controllers.advertiser.ajax import DAY
from faan.web.controllers.support import get_total_new_replies

from faan.x.util.timestamp import TimeStamp
from faan.x.util.func import try_int

from faan.core.model.security.account import Account


HOUR = 3600
DAY = HOUR * 24


def url_append(url, params=None, params2=None):
    if params is None:
        params = []
    if params2 is None:
        params2 = []
    if type(params) == dict:
        params = params.items()
    if type(params2) == dict:
        params2 = params2.items()
    qs = urllib.urlencode(dict(params + params2))
    return urlunparse(['', '', url, '', qs, '', ])


adv_menu = Menu([
    # Index
    MenuItem(u"Главная", url="/adv/", icon="home"),
    MenuItem(u"Новости", url="/adv/news/", icon="inbox"),

    # Campaigns
    MenuItem(u"Кампании", url="/adv/campaign/", icon="bullhorn", sub_items=[
        MenuItem(u"Новая кампания", url="/adv/campaign/new", icon="plus"),
        MenuItem(u"Список кампаний", url="/adv/campaign", icon="list")
    ]),

    # Advertiser statistics
    MenuItem(u"Статистика", url="/adv/stat", icon="bar-chart-o", sub_items=[
        MenuItem(u"По дате", url="/adv/stat/date", icon="calendar"),
        MenuItem(u"По кампаниям", url="/adv/stat/campaign", icon="bullhorn"),
        MenuItem(u"По медиа", url="/adv/stat/media", icon="video-camera"),
        MenuItem(u"По странам", url="/adv/stat/country", icon="globe"),
        MenuItem(u"По регионам", url="/adv/stat/region", icon="bank"),
        MenuItem(u"По городам", url="/adv/stat/city", icon="building"),
    ]),

    # Advertiser profile
    MenuItem(u"Профиль", url="/adv/profile", icon="user"),

    # Advertiser balance
    MenuItem(u"Финансы", url="/adv/balance", icon="ruble", sub_items=[
        MenuItem(u"История платежей", url="/adv/balance/history", icon="info"),
        MenuItem(u"Пополнить баланс", url="/adv/balance", icon="plus"),
    ]),

    # Advertiser support
    MenuItem(u"Поддержка", url="/adv/support", icon="life-ring", extra_html=lambda: sum(
        [v for k, v in dict(get_total_new_replies()).iteritems()]) or ""),
])

pub_menu = Menu([
    # Index
    MenuItem(u"Главная", url="/pub/", icon="home"),
    MenuItem(u"Новости", url="/pub/news/", icon="inbox"),

    # Apps
    MenuItem(u"Площадки", url="/pub/application/", icon="rocket",
             sub_items=[
                 MenuItem(u"Новая площадка", url="/pub/application/new", icon="plus"),
                 MenuItem(u"Список площадок", url="/pub/application", icon="list"),
                 MenuItem(u"Скачать SDK", url="/pub/application/sdk", icon="download"),
             ]),

    # Publister statistics
    MenuItem(u"Статистика", url="/pub/stat", icon="bar-chart-o",
             sub_items=[
                 MenuItem(u"По дате", url="/pub/stat/date", icon="calendar"),
                 MenuItem(u"По площадкам", url="/pub/stat/application", icon="bullhorn"),
                 MenuItem(u"По блокам", url="/pub/stat/unit", icon="video-camera"),
                 MenuItem(u"По источникам", url="/pub/stat/source", icon="external-link"),
                 MenuItem(u"По странам", url="/pub/stat/country", icon="globe"),
                 MenuItem(u"По регионам", url="/pub/stat/region", icon="bank"),
                 MenuItem(u"По городам", url="/pub/stat/city", icon="building"),
             ]),

    # Publisher profile
    MenuItem(u"Профиль", url="/pub/profile", icon="user"),

    # Publisher balance
    MenuItem(u"Финансы", url="/pub/balance", icon="ruble", sub_items=[
        MenuItem(u"История платежей", url="/pub/balance/history", icon="info"),
        MenuItem(u"Платежная информация", url="/pub/profile/#pay-info", icon="credit-card"),
    ]),

    # Publisher support
    MenuItem(u"Поддержка", url="/pub/support", icon="life-ring",
             extra_html=lambda: sum(
                 [v for k, v in dict(get_total_new_replies()).iteritems()]) or ""),
])


def divbyz(x, y):
    try:
        perc = float(x) / y
    except Exception:
        perc = 0
    return perc


def limitstr(str, lim):
    return str if len(str) < lim else str[:(lim - 2)] + "..."


def get_user_group(user):
    u = Account.Get(user)
    return u.groups


# def query_to_dict(query):
#     cls = None
#     d = []
#     for obj in query:
#         if isinstance(obj.__class__, DeclarativeMeta):
#             if not cls:
#                 cls = obj.__class__
#
#             json_ready_obj = dict((c, getattr(obj, c)) for c in [x.name for x in cls.__table__.columns])
#         elif isinstance(obj, KeyedTuple):
#             json_ready_obj = dict(zip(obj.keys(), obj))
#
#         # for field, value in json_ready_obj.iteritems():
#         #     if isinstance(value, Decimal):
#         #         json_ready_obj[field] = float(value)
#
#         d.append(json_ready_obj)
#
#     return d
#

class SAJson(object):
    @staticmethod
    def serialize(query, sort=None, ts_fields=None):
        cls = None
        json_ready_query = []
        for obj in query:
            if isinstance(obj.__class__, DeclarativeMeta):
                if not cls:
                    cls = obj.__class__

                json_ready_obj = dict((c, getattr(obj, c)) for c in [x.name for x in cls.__table__.columns])
            elif isinstance(obj, KeyedTuple):
                json_ready_obj = dict(zip(obj.keys(), obj))

            json_ready_query.append(json_ready_obj)

        return json_ready_query


def get_date_limits(r):
    """
    Get date limits from provided request

    Returns start and end dates as timestamp, True/False if hourly, timezone
    """

    user_timezone = int(r.values.get("timezone", -time.timezone))

    if r.values.get("date"):
        start, end = r.values["date"].split(" - ")
    else:
        start = r.values.get("from")
        end = r.values.get("to")

    try:
        start = int(time.mktime(datetime.datetime.strptime(start, "%d.%m.%Y").timetuple()))
        end = int(time.mktime(datetime.datetime.strptime(end, "%d.%m.%Y").timetuple()))
    except:
        start = int(time.mktime(datetime.datetime.combine(datetime.date.today(), datetime.time(0, 0)).timetuple()))
        end = int(time.mktime(datetime.datetime.combine(datetime.date.today(), datetime.time(0, 0)).timetuple()))

    if start - end == 0 or r.values.get("force_hourly", "false") == "true":
        hourly = True
    else:
        hourly = False

    return start - user_timezone, end - user_timezone + DAY - 1, hourly, user_timezone


def format_ts(ts, format="%d.%m.%Y"):
    return datetime.datetime.fromtimestamp(ts).strftime(format)


def make_graph_series(ts_list):
    return {ts: 0.0 for ts in ts_list}


def make_table_series(item_template):
    item_template = {f: 0 for f in item_template}
    table_series = {}
    for ts in get_ts_list(request):
        item = {"ts_spawn": ts}
        item.update(item_template)
        table_series[ts] = item
    return table_series


def graph_series_to_list(graph_series, tz=0, hourly=False):
    return sorted([[((ts + tz) * 1000), float(value)] for ts, value in graph_series.iteritems()], key=lambda x: x[0])


def table_series_to_list(table_series, tz=0, hourly=False):
    for ts, item in table_series.iteritems():
        item.update({"ts_spawn": item.get("ts_spawn") + tz})

    return sorted(table_series.values(), key=lambda x: x.get("ts_spawn"))


def get_ts_list(r=None):
    r = r or request
    start, end, hourly, tz = get_date_limits(r)
    ts_list = [
        start + (delta * (HOUR if hourly else DAY))
        for delta in range(
            (end + 1 - start) / (HOUR if hourly else DAY)
        )
    ]
    return ts_list
