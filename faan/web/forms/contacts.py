# coding=utf-8
from flask.ext.wtf import RecaptchaField
from wtforms import StringField, TextField
from wtforms.validators import input_required, Email
from faan.web.forms import XForm
from faan.web.forms.widgets import RecaptchaWidget

__author__ = 'limeschnaps'


class ContactForm(XForm):
    name = StringField(u"Имя", validators=[input_required(message=u"Обязательное поле")])
    email = StringField(u"Email", validators=[Email(message=u"Неправильный адрес электронной почты"), input_required(message=u"Обязательное поле")])
    phone = StringField(u"Телефон", validators=[input_required(message=u"Обязательное поле")])
    message = TextField(u"Сообщение", validators=[input_required(message=u"Обязательное поле")])
    captcha = RecaptchaField(u"Проверка", widget=RecaptchaWidget(
        theme='white',
        lang='ru'
    ))