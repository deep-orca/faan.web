# -*- coding: utf-8 -*-
from flask import g, request, redirect, jsonify
from faan.core.X.xflask import Controller, Route, capp
from faan.core.X.mako import render_template
from faan.core.model.meta import Session
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned
from faan.x.util import try_int

from faan.core.model.adv.statistics import AdvStat
from faan.x.util.timestamp import TimeStamp
import time
from faan.core.model.adv.campaign import Campaign
from faan.core.model.adv.media import Media

from datetime import datetime


@CProtect(IsSigned())
@Controller('/adv/stat')
class AdvStatController():

    @Route("/", methods=["GET", "POST"])
    @Route("/<group>", methods=["GET", "POST"])
    def index(self, group = "date"):
        g.group     = group or "date"
        g.campaign  = request.values.get('campaign')
        g.media     = request.values.get('media')
        try:
            g.date_period = request.values['date']
            g.ts, g.te = request.values['date'].split(' - ')
            g.ts = int(time.mktime(datetime.strptime(g.ts, "%d.%m.%Y").timetuple()))
            g.te = int(time.mktime(datetime.strptime(g.te, "%d.%m.%Y").timetuple()))
        except (KeyError, IndexError, ValueError, NameError, TypeError):
            g.ts = TimeStamp.today_tz() + time.timezone
            g.te = TimeStamp.today_tz() + time.timezone

        return render_template('/adv/stat/index.mako')
