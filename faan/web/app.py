from faan.core.X.xflask import XFlask
from faan.core.X.mako import init_mako
from faan.core.X.xemail import init_email
from . import controllers

from werkzeug.local import LocalStack, LocalProxy
from flask import request

from faan.core.model.security import Account


def get_account():
    account = None
    uid = request.environ.get("REMOTE_USER")
    if uid:
        account = Account.Get(uid)
    return account


account = LocalProxy(get_account)


def make_app(global_conf, **app_conf):
    app = XFlask(__name__, static_folder=app_conf['STATIC_FOLDER'])
    app.debug = True
    app.find_controllers(app.root_path + "/controllers/", controllers.__name__)
    app.config.update(app_conf)
    init_email(app)
    init_mako(app)
    return app.wsgi_app

if __name__ == "__main__":
    app = make_app(None)

    app.run()