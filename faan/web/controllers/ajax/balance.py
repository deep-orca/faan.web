# -*- coding: utf-8 -*-

from flask import g, request, redirect, make_response, jsonify
from sqlalchemy.sql.expression import desc
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.util._collections import KeyedTuple
from faan.core.X.xflask import Controller, Route, Error, capp, getCurrentIP
from faan.core.X.mako import render_template
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned

from faan.core.model.security import Account
from faan.core.model.adv.statistics import AdvStat
from faan.core.model.adv.campaign import Campaign
from faan.core.model.adv.media import Media
from faan.core.model import Category
from faan.core.model.meta import Session
from faan.core.model.transaction import Transaction
import decimal
import time
import operator
from datetime import datetime
from faan.x.util import try_int


class SAJsonSerializer(object):
    def __init__(self):
        pass

    @classmethod
    def serialize(cls, sa_query, sort=None, ts_fields=None, tz=0):
        """
        Decimal        -> Float
        Unix timestamp -> JS timestamp

        Returns JSON serializeable dict
        """
        if not ts_fields:
            ts_fields = []

        result = []

        for o in sa_query:
            if not isinstance(o, KeyedTuple):
                _obj = o.__dict__
                try:
                    del _obj["_sa_instance_state"]
                except KeyError:
                    pass
                result.append(_obj)
            else:
                result.append(dict(zip(o.keys(), o)))

        result = [{k: float(v) if isinstance(v, decimal.Decimal) else v if k not in ts_fields else int("%s000" % (v - tz)) for k, v in o.iteritems()} for o in result]
        if sort:
            sort_field, sort_order = sort
            result = sorted(result, key=operator.itemgetter(sort_field))
            if sort_order == -1:
                result.reverse()

        return result

#def prejson_dict(l):
#    return [{k: float(v) if isinstance(v, decimal.Decimal) else v for k, v in d.iteritems()} for d in l]


#def sort(q, cond):
#    return sorted([x._asdict() for x in q], key=operator.itemgetter(cond))

#def ts_utc_json(ts, tz=0):
#    if tz:
#        ts -= tz
#    return int("%d000" % ts)


@CProtect(IsSigned())
@Controller("/ajax/balance")
class AjaxController(object):
    @Route("/get")
    def get(self):
        """
        Returns balance of current user
        """
        uid = request.environ["REMOTE_USER"]
        try:
            account = Session().query(Account).filter(Account.id == uid).one()
        except MultipleResultsFound, e:
            return jsonify({"result": {"state": "error"}})

        balance = account.balance

        data = {
            "result": {
                "state": "ok",
                "balance": balance
            }
        }

        return jsonify(**data)

    @Route("/callback/<int:psid>")
    def callback(self, psid=0):
        """
        Recieving callback from pay system
        Processing callback and calling `self.deposit`
        """
        # TODO: Process callback in a proper way (depends on pay system)

        return self.deposit(40, 40, psid, 150, comment="Deposit from {0} PS".format(psid), ttype=2)

    #@Route("/deposit")
    def deposit(self, issuer, dst, psid, amount, comment="", reference=0, wallet="", ttype=0):
        """
        Deposits money for current user
        """
        try:
            account = Session().query(Account).filter(Account.id == dst).one()
        except MultipleResultsFound, e:
            return jsonify({"result": {"state": "error"}})

        t = Transaction.Default()
        t.amount = amount
        t.dst_acc = dst
        t.comment = comment
        t.issuer_acc = issuer
        t.ps = psid
        t.wallet = wallet
        t.reference = reference
        t.ts_spawn = time.mktime(datetime.utcnow().timetuple())
        t.type = ttype

        try:
            Session.add(t)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("EXCEPTION IN deposit ON transaction")
            return jsonify({"result": {"state": "error"}})

        account.balance += amount

        try:
            Session.add(account)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("EXCEPTION IN deposit ON account")
            return jsonify({"result": {"state": "error"}})

        data = {
            "result": {"state": "ok"}
        }
        return jsonify(**data)

    @Route("/list")
    def list_transactions(self):
        """
        Returns a list of transactions of current user
        """
        uid = request.environ["REMOTE_USER"]
        ltype = try_int(request.values.get("type"), -1)

        objects = []

        if ltype is not None:
            if ltype == 0:
                objects = Session.query(Transaction).filter(Transaction.amount >= 0, Transaction.dst_acc == uid, Transaction.type != 1).all()
            elif ltype == 1:
                objects = Session.query(Transaction).filter(Transaction.amount < 0, Transaction.dst_acc == uid, Transaction.type != 1).all()
            else:
                objects = Session.query(Transaction).filter(Transaction.dst_acc == uid, Transaction.type != 1).all()

        data = {
            "meta": {},
            "objects": SAJsonSerializer.serialize(objects, ts_fields=["ts_spawn", ], sort=("ts_spawn", -1))
        }
        return jsonify(**data)