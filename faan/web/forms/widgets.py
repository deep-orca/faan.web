# -*- coding: utf-8 -*-
from gettext import gettext as _
from pprint import pprint

from flask import json, current_app
from flask.ext.wtf.recaptcha.widgets import _JSONEncoder, RecaptchaWidget, RECAPTCHA_API_SERVER, RECAPTCHA_SSL_API_SERVER, RECAPTCHA_API_SERVER
from markupsafe import Markup, text_type
from werkzeug.urls import url_encode
from wtforms.widgets import TextInput, TextArea, Select, SubmitInput, html_params, HTMLString

RECAPTCHA_HTML = u'''
<script type="text/javascript">var RecaptchaOptions = %(options)s;</script>
<script type="text/javascript" src="%(script_url)s"></script>
<noscript>
  <div><iframe src="%(frame_url)s" height="300" width="500"></iframe></div>
  <div>
    <textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
    <input type="hidden" name="recaptcha_response_field"
           value="manual_challenge" />
  </div>
</noscript>
'''


class RecaptchaWidget(object):
    def __init__(self, html="", **options):
        self.options = {
            'theme': 'clean',
            'custom_translations': {
                'visual_challenge': _('Get a visual challenge'),
                'audio_challenge': _('Get an audio challenge'),
                'refresh_btn': _('Get a new challenge'),
                'instructions_visual': _('Type the two words:'),
                'instructions_audio': _('Type what you hear:'),
                'help_btn': _('Help'),
                'play_again': _('Play sound again'),
                'cant_hear_this': _('Download sound as MP3'),
                'incorrect_try_again': _('Incorrect. Try again.'),
            }
        }
        self.html = html or RECAPTCHA_HTML
        self.options.update(options)

    def recaptcha_html(self, server, query, options):
        return Markup(self.html % dict(
            script_url='%schallenge?%s' % (server, query),
            frame_url='%snoscript?%s' % (server, query),
            options=json.dumps(options, cls=_JSONEncoder)
        ))

    def __call__(self, field, error=None, **kwargs):
        """Returns the recaptcha input HTML."""

        if current_app.config.get('RECAPTCHA_USE_SSL', False):
            server = RECAPTCHA_SSL_API_SERVER
        else:
            server = RECAPTCHA_API_SERVER

        try:
            public_key = current_app.config['RECAPTCHA_PUBLIC_KEY']
        except KeyError:
            raise RuntimeError("RECAPTCHA_PUBLIC_KEY config not set")
        query_options = dict(k=public_key)

        if field.recaptcha_error is not None:
            query_options['error'] = text_type(field.recaptcha_error)

        query = url_encode(query_options)

        return self.recaptcha_html(server, query, self.options)


class BSButtonGroupWidget(object):
    """

    """

    def __init__(self, disabled_options=None):
        self.html_tag = "div"
        self.disabled_options = disabled_options or []

    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        hook_cls = 'btn-{field}-action'.format(field=field.name)
        html = ["<%s class='btn-group btn-group-justified' role='group' %s>" % (self.html_tag, html_params(**kwargs))]
        for subfield in field:
            html.append("<a href='#' class='btn btn-default {hook_cls} {checked}' data-value='{value}'>{label}</a>".format(
                hook_cls='disabled' if (subfield.object_data in self.disabled_options) else hook_cls,
                value=subfield.object_data,
                label=subfield.label.text,
                checked=('active' if subfield.object_data == field.data else '')
            ))
        html.append('</%s>' % self.html_tag)
        html.append('<input type="hidden" name="{field}" id="{field}_data" value="{value}"/>'.format(field=field.name, value=field.data))
        html.append('<script type="text/javascript">'
                    '$(".%(hook_cls)s").on("click",function(evt){'
                    'evt.preventDefault();'
                    '$("#%(field)s_data").val($(this).data("value"));'
                    # 'console.debug("Value changed -> ", $("#%(field)s_data").val());'
                    '$(".%(hook_cls)s").removeClass("active");'
                    '$(this).addClass("active");})'
                    '</script>' % ({
                        'field': field.name,
                        'hook_cls': hook_cls
                    }))
        return HTMLString(''.join(html))


class BSWidgetMixin(object):
    def __init__(self, *args, **kwargs):
        super(BSWidgetMixin, self).__init__(*args, **kwargs)

    def __call__(self, field, form_cls=None, label_width=None, field_width=None, **kwargs):
        if label_width and not field_width:
            field_width = 12 - label_width
        elif not label_width and field_width:
            label_width = 12 - field_width
        elif not label_width and not field_width:
            label_width = 3
            field_width = 9

        _w_cls_name = type(self).__name__
        kwargs["class"] = kwargs.pop("class_", "") or kwargs.pop("class", "")
        kwargs["class"] += " " + ("form-control" if _w_cls_name != "BSSubmit" else "btn btn-success")

        has_label = kwargs.pop("has_label", True)

        base_widget = super(BSWidgetMixin, self).__call__(field, **kwargs)

        html = u'''
        <div class="form-group %(error_cls)s">
            %(label)s
            <div class="%(field_cls)s">
                %(widget)s

                <div class="x-form-error text-danger">%(errors)s</div>
            </div>
        </div>
        ''' % ({
            "widget": base_widget,

            "label": (u"<label for='%(id)s' class=%(label_cls)s control-label'>%(label)s</label>" %
            ({
                 "id": field.id,
                 "label": field.label.text,
                 "label_cls": ("col-sm-%d" % label_width) if form_cls == "form_horizontal" else ""
            })) if has_label else u"",
            "errors": "<br/>".join(field.errors or []),
            "error_cls": "has-error" if field.errors else "",
            "field_cls": ("col-sm-%d" % field_width) if form_cls == "form_horizontal" else ""
        })

        return html


class BSTextInput(BSWidgetMixin, TextInput): pass


class BSTextArea(BSWidgetMixin, TextArea): pass


class BSSelect(BSWidgetMixin, Select): pass


class BSSubmit(BSWidgetMixin, SubmitInput): pass





