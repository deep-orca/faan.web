# -*- coding: utf-8 -*-
from flask import request
from faan.core.X.xflask import Controller, Error
from faan.core.X.mako import render_template


@Controller
class ErrorController():
    @Error(404)
    def page_404(self, e):
        if request.environ.get('REMOTE_USER'):
            return render_template('/error/404_loged.mako'), 404
        else:
            return render_template('/error/404.mako'), 404

    @Error(403)
    def page_403(self, e):
        if request.environ.get('REMOTE_USER'):
            return render_template('/error/500_loged.mako'), 403
        else:
            return render_template('/error/500.mako'), 403

