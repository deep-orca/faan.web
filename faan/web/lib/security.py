
from repoze.what.predicates import Predicate
from faan.core.model.security import Account


def get_account_id(environ):
    account_id = environ.get('repoze.who.identity', {}).get('repoze.who.userid', None)
    return account_id


def get_account(environ, account_id=None):
    if not account_id:
        account_id = get_account_id(environ)

    account = Account.Get(account_id)

    return account


class IsSigned(Predicate):
    message = "Stop right there, criminal scum!"
    
    def __init__(self, **kwargs):
        Predicate.__init__(self, **kwargs)
    
    def evaluate(self, environ, credentials):
        if not environ.get('repoze.who.identity', {}).get('repoze.who.userid', None):
            self.unmet()


class HasGroup(Predicate):
    message = "Halt! Du bist ein schmetterling!"

    def __init__(self, account_group, *account_groups, **kwargs):
        Predicate.__init__(self, **kwargs)
        self.account_groups = [account_group] + list(account_groups)

    def evaluate(self, environ, credentials):
        account_id = get_account_id(environ) or self.unmet()
        account = get_account(environ) or self.unmet()
        account.groups in self.account_groups or self.unmet()


class NotOwnedException(Exception):
    pass


class NotAllowedException(Exception):
    pass