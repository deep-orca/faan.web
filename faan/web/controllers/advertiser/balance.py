from flask import g, request, redirect
from sqlalchemy.sql.expression import desc

from faan.core.X.xflask import Controller, Route, Error, capp, getCurrentIP
from faan.core.X.mako import render_template
from faan.web.app import account
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned

from faan.core.model.security import Account

@CProtect(IsSigned())
@Controller("/adv/balance")
class AdBalanceController(object):
    @Route("/")
    def index(self):
        g.account = account
        return render_template('adv/balance/form.mako')

    @Route("/history/")
    def history(self):
        g.account = account
        return render_template('adv/balance/history.mako')