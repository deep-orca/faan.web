# -*- coding: utf-8 -*-
from pprint import pprint
import urlparse
from flask import request, jsonify
from faan.core.X.xflask import Controller, Route
from faan.core.model.adv.campaign import Campaign
from faan.core.model.general.geo import GeoCountry, GeoRegion, GeoCity
from faan.core.model.pub.source import Source
from faan.core.model.pub.unit import Unit
from faan.web.lib.helpers import SAJson, divbyz, get_date_limits, get_ts_list, make_graph_series, format_ts, \
    graph_series_to_list, make_table_series, table_series_to_list
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned
from faan.core.model.pub.statistics import PubStat
from faan.core.model.pub.application import Application
from faan.core.model.pub.zone import Zone
from faan.core.model.meta import Session
from faan.core.model.category import Category

import requests
from faan.x.util import try_int


def process_url(url, keep_params=('CONTENT_ITEM_ID=',)):
    parsed = urlparse.urlsplit(url)
    filtered_query = '&'.join(q_item for q_item in parsed.query.split('&') if q_item.startswith(keep_params))
    return urlparse.urlunsplit(parsed[:3] + (filtered_query,) + parsed[4:])


class ContentRating(object):
    rating = None

    def __init__(self, rating):
        if rating == "Low Maturity" or rating == "4+" or rating == "9+":
            self.rating = 1
        elif rating == "Medium Maturity" or rating == "12+":
            self.rating = 2
        elif rating == "High Maturity" or rating == "17+":
            self.rating = 3
        else:
            self.rating = 0


@CProtect(IsSigned())
@Controller("/pub/ajax")
class IndexController(object):
    # @Route("/test")
    # def test(self):
    #     user = request.environ['REMOTE_USER']
    #     app = Session.query(Application).filter(Application.account == user).first()
    #
    #     print "App ID:", app.id
    #
    #     q1 = Session.query(Campaign).filter((Campaign.target_applications.any(app.id)) | (Campaign.target_applications == [])).all()
    #     q2 = Session.query(Campaign).filter((~Campaign.target_applications.any(app.id)) | (Campaign.target_applications == [])).all()
    #
    #     print [x.id for x in q1], 168 in [x.id for x in q1]
    #     print [x.id for x in q2], 168 not in [x.id for x in q2]
    #
    #
    #
    #     return jsonify({'data': 'ok'})
    # Dashboards
    @Route("/dashboard/table", methods=["GET", "POST"])
    def dashboard(self):
        user = request.environ['REMOTE_USER']

        start, end, hourly, tz = get_date_limits(request)

        query = PubStat.PreQuery()\
                   .filter(PubStat.ts_spawn >= start) \
                   .filter(PubStat.ts_spawn < end)\
                   .filter(PubStat.account == user)

        query = query.add_column(PubStat.application).group_by(PubStat.application)

        query_result = SAJson.serialize(query.all(), sort=('application', 1))

        app_names = {app.id: app.name for app in Application.Filter(account=user).all()}

        for item in query_result:
            item.update({
                "name": app_names.get(item.get("application")),
                "revenue": item.get("revenue")
            })

        data = {
            "meta": {
                "date_from": format_ts(start),
                "date_to": format_ts(end)
            },
            "objects":  query_result
        }

        return jsonify(**data)

    @Route("/dashboard/graph/overview", methods=["GET", "POST"])
    def dashboard_graph(self):
        user = request.environ['REMOTE_USER']

        start, end, hourly, tz = get_date_limits(request)

        grouping = PubStat.Presets.GROUP_HOURLY if hourly else PubStat.Presets.GROUP_DAILY_TZ(tz)

        query = PubStat.PreQuery() \
            .filter(PubStat.ts_spawn >= start) \
            .filter(PubStat.ts_spawn <= end) \
            .filter(PubStat.account == user)\
            .add_column(grouping.label('ts_spawn'))\
            .group_by(grouping)

        ts_list = get_ts_list(request)

        graphs = {gname: make_graph_series(ts_list) for gname in ["revenue", "impressions"]}

        for x in query.all():
            for gname, gs in graphs.iteritems():
                if start <= x.ts_spawn <= end:
                    graphs[gname][x.ts_spawn - (tz if not hourly else 0)] = float(getattr(x, gname))

        data = {
            "meta": {
                "hourly": hourly,
                "date_from": format_ts(start),
                "date_to": format_ts(end)
            },
            "objects": [
                {
                    "label": u"Заработок",
                    "data": graph_series_to_list(graphs["revenue"], tz, hourly)
                },
                # {
                #     "label": u"Просмотров",
                #     "data": graph_series_to_list(graphs["raw"], tz, hourly)
                # },
                {
                    "label": u"Показов",
                    "data": graph_series_to_list(graphs["impressions"], tz, hourly)
                }
            ]
        }

        return jsonify(**data)

    @Route("/dashboard/graph/revenue", methods=["GET", "POST"])
    def dashboard_graph_revenue(self):
        user = request.environ['REMOTE_USER']

        start, end, hourly, tz = get_date_limits(request)

        grouping = PubStat.Presets.GROUP_HOURLY if hourly else PubStat.Presets.GROUP_DAILY_TZ(tz)

        query = PubStat.PreQuery() \
            .filter(PubStat.ts_spawn >= start) \
            .filter(PubStat.ts_spawn < end) \
            .filter(PubStat.account == user) \
            .add_column(grouping.label('ts_spawn')) \
            .add_column(PubStat.application).group_by(grouping, PubStat.application)

        app_names = {app.id: app.name for app in Application.Filter(account=user).all()}

        apps = {}
        for x in query.all():
            if not apps.get(x.application):
                apps[x.application] = []

            apps[x.application].append(x)

        ts_list = get_ts_list(request)

        obj_list = []
        for app_id, app_data in apps.iteritems():
            graph_series = make_graph_series(ts_list)
            for cd in sorted(app_data, key=lambda x: x.ts_spawn):
                if start <= cd.ts_spawn <= end:
                    graph_series[cd.ts_spawn - (tz if not hourly else 0)] = cd.revenue

            obj_list.append({
                "label": app_names.get(app_id, ""),
                "data": graph_series_to_list(graph_series, tz, hourly)
            })

        data = {
            "meta": {
                "hourly": hourly,
                "date_from": format_ts(start),
                "date_to": format_ts(end)
            },
            "objects": obj_list or graph_series_to_list(make_graph_series(ts_list))
        }

        return jsonify(**data)

    # Statistics
    @Route("/application", methods=["GET"])
    def apps_list(self):
        user = request.environ["REMOTE_USER"]
        obj_list = [{"id": None, "name": u"Все"}]

        search = request.values.get("q")

        q = Session.query(Application)\
            .filter(Application.account == user)

        if search and search is not "":
            q = q.filter(Application.name.ilike(u"%{0}%".format(search)))

        q = q.all()

        obj_list += [{"id": a.id, "name": a.name} for a in q]

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/application/<int:app_id>/zone", methods=["GET"])
    def zones_list(self, app_id=0):
        """
        WARNING: DEPRECATED

        Return zones list
        """
        user = request.environ["REMOTE_USER"]
        obj_list = [{"id": None, "name": u"Все"}]

        search = request.values.get("q")

        if app_id:
            try:
                _app_id = Session.query(Application.id).filter(Application.id == app_id, Application.account == user).all()[0].id

                q = Session.query(Zone.id, Zone.name)\
                    .filter(Zone.application == _app_id)

                if search and search is not "":
                    q = q.filter(Zone.name.ilike(u"%{0}%".format(search)))

                q = q.all()

                obj_list += [{"id": z.id, "name": z.name} for z in q]
            except IndexError:
                print 'ERROROROROROROROROROOR'

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/application/<int:app_id>/unit", methods=["GET"])
    def units_list(self, app_id=0):
        """
        Return unit list
        """
        user = request.environ["REMOTE_USER"]
        obj_list = [{"id": None, "name": u"Все"}]

        search = request.values.get("q")

        if app_id:
            try:
                _app_id = Session.query(Application.id).filter(Application.id == app_id, Application.account == user).all()[0].id

                q = Session.query(Unit.id, Unit.name) \
                    .filter(Unit.application == _app_id)

                if search and search is not "":
                    q = q.filter(Unit.name.ilike(u"%{0}%".format(search)))

                q = q.all()

                obj_list += [{"id": z.id, "name": z.name} for z in q]
            except IndexError:
                print 'Error in getting unit list'

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/application/<int:app_id>/unit/<int:unit_id>/source", methods=["GET"])
    def sources_list(self, app_id=0, unit_id=0):
        """
        Return unit list
        """
        user = request.environ["REMOTE_USER"]
        obj_list = [{"id": None, "name": u"Все"}]

        search = request.values.get("q")

        if app_id:
            _app_id = Session.query(Application.id).filter(Application.id == app_id, Application.account == user).all()[0].id
            _unit_id = Session.query(Unit.id).filter(Unit.id == unit_id, Unit.application == _app_id).all()[0].id

            q = Session.query(Source.id, Source.name) \
                .filter(Source.unit == _unit_id)

            if search and search is not "":
                q = q.filter(Source.name.ilike(u"%{0}%".format(search)))

            q = q.all()

            obj_list += [{"id": z.id, "name": z.name} for z in q]

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/stat", methods=["GET", "POST"])
    @Route("/stat/<group>", methods=["GET", "POST"])
    def stat(self, group="date"):
        user      = request.environ["REMOTE_USER"]
        group     = group or "date"
        application  = try_int(request.values.get('application'), None)
        # zone     = request.values.get('zone')
        unit     = try_int(request.values.get('unit'), None)
        source   = try_int(request.values.get('source'), None)

        country = try_int(request.values.get('country'), None)
        region = try_int(request.values.get('region'), None)
        city = try_int(request.values.get('city'), None)

        # print country, region, city, application, zone

        app_names = country_names = region_names = city_names = unit_names = source_names = {}

        start, end, hourly, tz = get_date_limits(request)

        q = PubStat.PreQuery()\
                   .filter(PubStat.ts_spawn >= start) \
                   .filter(PubStat.ts_spawn < end)\
                   .filter(PubStat.account == user)

        if application:
            q = q.filter(PubStat.application == application)
        # if zone:
        #     q = q.filter(PubStat.zone == zone)
        if unit:
            q = q.filter(PubStat.unit == unit)
        if source:
            q = q.filter(PubStat.source == source)
            print q.all()
        if country:
            q = q.filter(PubStat.country == country)
        if region:
            q = q.filter(PubStat.region == region)
        if city:
            q = q.filter(PubStat.city == city)

        if group == 'date':
            grouping = PubStat.Presets.GROUP_HOURLY if hourly else PubStat.Presets.GROUP_DAILY_TZ(-tz)
            q = SAJson.serialize(q.add_column(grouping.label('ts_spawn')).group_by(grouping).all())
            # table_series = make_table_series(
            #     ['cost', 'impressions', 'raw', 'cpm', 'raw_impressions'])
            # table_series.update({i.get("ts_spawn") - (tz if not hourly else 0): i for i in q})
            # q = table_series_to_list(table_series, tz=tz)
        elif group == 'application':
            app_names = {x.id: x.name for x in Application.Filter(account=user).all()}
            q = q.add_column(PubStat.application).group_by(PubStat.application)
            q = SAJson.serialize(q.all(), sort=('application', 1))
        elif group == 'unit':
            q = q.add_column(PubStat.unit).group_by(PubStat.unit)
            q = SAJson.serialize(q.all(), sort=('unit', 1))
            unit_names = {x.id: x.name for x in Unit.All()}
        elif group == 'source':
            q = q.add_column(PubStat.source).group_by(PubStat.source)
            q = SAJson.serialize(q.all(), sort=('source', 1))
            source_names = {x.id: x.name for x in Source.All()}
        # elif group == 'zone':
        #     zone_names = {x.id: x.name for x in Zone.All()}
        #     zone_names.update({0: u"Другое"})
        #     q = q.add_column(PubStat.zone).group_by(PubStat.zone)
        #     q = SAJson.serialize(q.all(), sort=('zone', 1))
        elif group == 'country':
            q = q.add_column(PubStat.country).group_by(PubStat.country)
            q = SAJson.serialize(q.all(), sort=('country', 1), ts_fields=['ts_spawn'])
            country_names = {x.id: x.name for x in GeoCountry.Filter().all()}
            country_names.update({0: u"Другое"})
        elif group == 'region':
            q = q.add_column(PubStat.region).group_by(PubStat.region)
            q = SAJson.serialize(q.all(), sort=('region', 1), ts_fields=['ts_spawn'])
            region_names = {x.id: x.name for x in GeoRegion.Filter().all()}
            region_names.update({0: u"Другое"})
        elif group == 'city':
            q = q.add_column(PubStat.city).group_by(PubStat.city)
            q = SAJson.serialize(q.all(), sort=('city', 1), ts_fields=['ts_spawn'])
            city_names = {x.id: x.name for x in GeoCity.Filter().all()}
            city_names.update({0: u"Другое"})

        for item in q:
            item.update({
                "application_name": app_names.get(item.get("application")),
                # "zone_name": zone_names.get(item.get("zone")),
                "unit_name": unit_names.get(item.get("unit")),
                "source_name": source_names.get(item.get("source")),
                "country_name": country_names.get(item.get("country")),
                "region_name": region_names.get(item.get("region")),
                "city_name": city_names.get(item.get("city")),
                "cpm": (divbyz(float(item.get("revenue")), item.get("impressions")) * 1000),
                "cost": item.get("revenue"),
            })

        data = {
            "meta": {
                "group": group,
                "application": application,
                "unit": unit,
                "source": source,
                "date_from": format_ts(start),
                "date_to": format_ts(end),
                "hourly": hourly
            },
            "objects": q
        }

        return jsonify(**data)

    # App creation
    @Route("/category", methods=["GET"])
    def categories_list(self, app_id=0):
        user = request.environ["REMOTE_USER"]
        platform = request.values.get("platform")
        ctype = request.values.get("type")
        obj_list = []

        search = request.values.get("q")

        q = Session.query(Category.name, Category.id)

        if platform:
            q = q.filter(Category.platform == platform)

        if ctype:
            q = q.filter(Category.type == ctype)

        if search and search is not "":
            q = q.filter(Category.name.ilike(u"%{0}%".format(search)))

        q = q.all()

        obj_list += [{"id": z.id, "name": z.name} for z in q]

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/store/search", methods=["GET"])
    def search(self):
        search = unicode.encode(request.values.get("q"), 'utf-8')
        try:
            platform = int(request.values.get("platform"))
        except:
            platform = 0

        lang = request.values.get("lang")

        if platform == 1:
            try:
                url = "https://itunes.apple.com/search?term={0}&media=software".format(search)

                if lang:
                    url += "&country={0}".format(lang.lower())

                r = requests.get(url).json().get("results", [])

                data = {
                    "meta": {},
                    "objects": [
                        {
                            "name": o.get("trackName"),
                            "genres": o.get("genreIds"),
                            "rating": ContentRating(o.get("trackContentRating")).rating,
                            "icon": o.get("artworkUrl60"),
                            "url": o.get("trackViewUrl")
                        } for o in r
                    ]
                }

            except requests.ConnectionError, e:
                data = {"meta": {"error": "ConnectionError"}, "objects": []}

        elif platform == 2:
            try:
                r = requests.get("https://42matters.com/api/1/apps/search.json?access_token=7b97f9896d7b8a909d1eeb4742f8283849a65ec4&limit=10&q={0}".format(search)).json().get("results", [])

                data = {
                    "meta": {},
                    "objects": [
                        {
                            "name": o.get("title"),
                            "genres": [o.get("cat_int")],
                            "rating": ContentRating(o.get("content_rating")).rating,
                            "icon": o.get("icon_72"),
                            "url": process_url(o.get("market_url"), keep_params=('id=',))
                        } for o in r
                    ]
                }

            except requests.ConnectionError, e:
                data = {"meta": {"error": "ConnectionError"}, "objects": []}
        else:
            data = {"meta": {"error": "InvalidParams"}, "objects": []}

        return jsonify(**data)
