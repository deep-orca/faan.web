# -*- coding: utf-8 -*-

from flask import g, request, redirect, make_response, jsonify
from sqlalchemy.sql.expression import desc
from faan.core.X.xflask import Controller, Route, Error, capp, getCurrentIP
from faan.core.X.mako import render_template
from faan.core.model.news import News
from faan.core.model.meta import Session
from faan.core.model.tag import Tag
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned

from faan.web.app import account


@CProtect(IsSigned())
@Controller("/pub")
class IndexController(object):
    @Route("/")
    def index(self):
        g.priorities = {
            News.Priority.LOW: (u"Низкая", "info"),
            News.Priority.NORMAL: (u"Обычная", "success"),
            News.Priority.HIGH: (u"Высокая", "warning"),
            News.Priority.CRITICAL: (u"Критическая", "danger"),
        }

        g.news = Session.query(News).filter(News.for_group.in_([News.ForGroup.ALL, News.ForGroup.PUB])).order_by(
            -News.ts_created).all()[:3]
        g.tags = {t.id: t for t in
                  Session.query(Tag).filter(Tag.id.in_(list(set([t for n in g.news for t in n.tags])))).all()}

        return render_template('pub/index.mako', account=account)

    # Dashboard moved to ./ajax.py