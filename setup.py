import os
from setuptools import setup as xsetup, find_packages
x_pkg_name = os.path.basename(os.getcwd())
xsetup(
    name = 'faan.web',
    version = '0.3.8',
    description = '',
    author = '',
    author_email = '',
    url = '',
    namespace_packages = ['faan'],
    install_requires = [
        "Flask>=0.8",
        "SQLAlchemy>=0.7",
        "gaq-hub",
        "zope.interface>=4",
        "repoze.who>2",
        "pastedeploy",
        "paste",
        "PasteScript",
        "repoze.what",
        "flask-wtf",
        "wtforms"
    ],
    packages = find_packages(exclude = ['ez_setup']),
    include_package_data = True,
    test_suite = 'nose.collector',
    zip_safe = False,
    entry_points = """
    [paste.app_factory]
    main = faan.web.app:make_app
    """
)

