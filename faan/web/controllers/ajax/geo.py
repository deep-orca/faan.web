# coding=utf-8
from flask import request, jsonify
from faan.core.X.xflask import Controller, Route
from faan.core.model.general.geo import GeoCountry, GeoRegion, GeoCity
from faan.core.model.meta import Session
from faan.web.lib.security import IsSigned
from faan.x.security import CProtect

__author__ = 'limeschnaps'


@CProtect(IsSigned())
@Controller("/<string:account_type>/ajax")
class AjaxController(object):
    @Route("/country", methods=["GET"])
    def country_list(self, account_type):
        """
        Return country list based on enetered searching queue
        """
        obj_list = [{"id": None, "name": u"Все"}]

        search = request.values.get("q")

        q = Session.query(GeoCountry.id, GeoCountry.name)

        if search and search is not "":
            q = q.filter(GeoCountry.name.ilike(u"%{0}%".format(search)))

        q = q.all()

        obj_list += [{"id": c.id, "name": c.name} for c in q]
        obj_list.append({"id": 0, "name": u"Другое"})

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)

    @Route("/country/<int:country_id>/region", methods=["GET"])
    def region_list(self, account_type, country_id):
        """
        Return region list based on enetered searching queue and country
        """
        obj_list = [{"id": None, "name": u"Все"}]

        if country_id:
            search = request.values.get("q")

            try:
                _country_id = Session.query(GeoCountry.id).filter(GeoCountry.id == country_id).first().id
                q = Session.query(GeoRegion.id, GeoRegion.name) \
                    .filter(GeoRegion.country == _country_id)

                if search and search is not "":
                    q = q.filter(GeoRegion.name.ilike(u"%{0}%".format(search)))

                q = q.all()

                obj_list += [{"id": r.id, "name": r.name} for r in q]
                obj_list.append({"id": 0, "name": u"Другое"})
            except:
                pass

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)


    @Route("/country/<int:country_id>/region/<int:region_id>/city", methods=["GET"])
    def city_list(self, account_type, country_id, region_id):
        """
        Return city list based on enetered searching queue, country and region
        """
        obj_list = [{"id": None, "name": u"Все"}]

        if country_id and region_id:
            search = request.values.get("q")

            try:
                _country_id = Session.query(GeoCountry.id).filter(GeoCountry.id == country_id).first().id
                _region_id = Session.query(GeoRegion.id).filter(GeoRegion.country == _country_id,
                                                                GeoRegion.id == region_id).first().id
                q = Session.query(GeoCity.id, GeoCity.name) \
                    .filter(GeoCity.region == _region_id)

                if search and search is not "":
                    q = q.filter(GeoCity.name.ilike(u"%{0}%".format(search)))

                q = q.all()

                obj_list += [{"id": r.id, "name": r.name} for r in q]
                obj_list.append({"id": 0, "name": u"Другое"})
            except:
                pass

        data = {
            "meta": {},
            "objects": obj_list
        }
        return jsonify(**data)