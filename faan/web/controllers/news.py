# coding=utf-8

import datetime
import time
from flask import request, g, redirect
from sqlalchemy.dialects.postgresql import Any
from faan.core.X.xflask import Controller, Route, capp
from faan.core.X.mako import render_template
from faan.core.model.news import News
from faan.core.model.security import Account
from faan.core.model.meta import Session
from faan.core.model.tag import Tag
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned
from faan.x.util import try_int


priorities = {
    News.Priority.LOW: (u"Низкая", "info"),
    News.Priority.NORMAL: (u"Обычная", "success"),
    News.Priority.HIGH: (u"Высокая", "warning"),
    News.Priority.CRITICAL: (u"Критическая", "danger"),
}


@CProtect(IsSigned())
@Controller("/<string:account_group>/news")
class NewsController(object):
    @Route("/")
    def news_index_view(self, account_group):
        g.current_tag = try_int(request.values.get("tag"))
        groups = [News.ForGroup.ALL] + ([News.ForGroup.ADV] if account_group == "adv" else [News.ForGroup.PUB])
        g.account_group = account_group
        g.priorities = priorities

        query = Session.query(News).filter(News.for_group.in_(groups))

        all_news = query.order_by(-News.ts_created).all()

        if g.current_tag:
            query = query.filter(Any(g.current_tag, News.tags))

        g.news = query.order_by(-News.ts_created).all()
        g.tags = {t.id: t for t in Session.query(Tag).filter(Tag.id.in_(list(set([t for n in all_news for t in n.tags])))).all()}

        return render_template("news/index.mako")

    @Route("/<int:news_id>/")
    def news_detail_view(self, account_group, news_id=None):
        if not news_id:
            return redirect("/{account_group}/news/".format(account_group=account_group))

        groups = [News.ForGroup.ALL] + ([News.ForGroup.ADV] if account_group == "adv" else [News.ForGroup.PUB])
        g.account_group = account_group
        g.priorities = priorities
        g.news_item = Session.query(News).filter(News.for_group.in_(groups), News.id==news_id).first()
        if not g.news_item:
            return redirect("/{account_group}/news/".format(account_group=account_group))

        g.tags = {t.id: t for t in Session.query(Tag).filter(Tag.id.in_(list(set([t for t in g.news_item.tags])))).all()}

        return render_template("news/details.mako")