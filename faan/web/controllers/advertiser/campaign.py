# -*- coding: utf-8 -*-
from pprint import pprint
import traceback
from flask import g, request, redirect, jsonify, json, abort
from faan.core.X.xflask import Controller, Route, capp, Error
from faan.core.X.mako import render_template
from faan.web.app import account
from faan.web.forms.advertiser import CampaignForm
from faan.x.security import CProtect
from faan.web.lib.security import IsSigned, NotOwnedException, NotAllowedException
from faan.core.model.adv.campaign import Campaign, _Campaign_States
from faan.core.model.adv.media import Media
from faan.core.model.meta import Session
from faan.core.model.security.account import Account
from faan.core.model.category import Category
from decimal import Decimal
from faan.core.model.adv.forecast import ForecastStat
from sqlalchemy.sql.expression import asc, and_
from sqlalchemy.sql.functions import sum as sqlsum
from sqlalchemy import func as F
import time
from datetime import datetime
from faan.x.util import try_int
from faan.core.model.general.geo import Geo, GeoCountry, GeoRegion, GeoCity
from faan.web.lib.countrynodes import generateNodes


@CProtect(IsSigned())
@Controller('/adv/campaign')
class CampaignController():
    BASEBID = 1.0

    @Error(ex=(NotOwnedException, KeyError, NotAllowedException))
    def error(self, ex):
        return render_template("/error/404_loged.mako" if account else "/error/404.mako"), 404

    @Route("/")
    def index(self):
        g.campaigns = Campaign.All(account=account.id)
        g.campaign_states = _Campaign_States()
        q = Session.query(F.count(Media.id).label("count"), Media.campaign, Media.state) \
            .filter(Media.campaign.in_([x.id for x in g.campaigns])) \
            .group_by(Media.state, Media.campaign).all()

        _mstates = {
            0: "new",
            1: "active",
            2: "inactive",
            3: "banned"
        }

        _cmedia = {}
        for m in q:
            if not _cmedia.get(m.campaign):
                _cmedia[m.campaign] = {}
            _cmedia[m.campaign].update({_mstates.get(m.state): m.count})

        for c in g.campaigns:
            c.media = _cmedia.get(c.id, {})

        g.categories_dict = dict([(c.id, c) for c in Category.All()])

        return render_template('/adv/campaign/list.mako')


    @Route("/group_actions", methods=["POST"])
    def group_actions(self):
        campaign_list = request.values.get('objects_list')
        action = request.values.get('action')

        for m in campaign_list[:-1].split(','):
            if action == 'delete':
                self.delete(int(m))
            elif action == '1':
                self.activate(int(m))
            else:
                self.deactivate(int(m))

        return jsonify({'error': 0,
                        'action': action,
                        'campaign_list': campaign_list[:-1].split(',')})


    @Route("/<int:cmp_id>", methods=["GET", "POST"])
    @Route("/new", methods=["GET", "POST"])
    def form(self, cmp_id=None):
        g.campaign = Campaign.Single(id=cmp_id) or Campaign.Default()
        form = CampaignForm(request.form, obj=g.campaign)

        if cmp_id and g.campaign.account != account.id:
            raise NotOwnedException

        # print reduce(lambda acc, v: acc | int(v)
        #              , request.values.getlist('targeting')
        #              , 0), request.values.getlist('targeting')

        pprint(form.data)


        if request.method == "POST" and form.validate():

            print form.data

            g.campaign.name                     = form.data.get('name')
            g.campaign.category                 = form.data.get('category')

            g.campaign.limit_cost_total         = form.data.get('limit_cost_total')
            g.campaign.limit_cost_daily         = form.data.get('limit_cost_daily')

            g.campaign.ts_start                 = form.data.get('ts_start')
            g.campaign.ts_end                   = form.data.get('ts_end')

            g.campaign.targeting                = reduce( lambda acc, v: acc | int(v)
                                                        , request.values.getlist('targeting')
                                                        , 0)

            g.campaign.target_categories_groups = form.data.get('target_categories_groups', [])

            campaign_categories = Session.query(Category.related_categories).filter(Category.id.in_(g.campaign.target_categories_groups)).all()

            target_categories = []
            for c in campaign_categories:
                target_categories += c.related_categories

            g.campaign.target_categories        = list(set(target_categories))
            g.campaign.target_platforms         = form.data.get('target_platforms', [])
            g.campaign.target_devclasses        = form.data.get('target_devclasses', [])
            g.campaign.target_geo               = form.data.get('target_geo', [])
            # print form.data.get('target_content_rating'), form.target_content_rating.data or [0,1,2,3]
            g.campaign.target_content_rating    = form.target_content_rating.data or [0, 1, 2, 3]
            g.campaign.target_age               = form.target_age.data
            g.campaign.target_gender            = form.target_gender.data
            g.campaign.target_applications      = form.target_applications.data

            g.campaign.bid                      = Decimal(form.data.get('bid'))
            g.campaign.payment_type             = form.payment_type.data or 1
            g.campaign.total_cost_limit         = request.values.get('total_cost_limit')

            g.campaign.account                  = request.environ['REMOTE_USER']
            g.campaign.type                     = g.campaign.type or Campaign.Type.REGULAR
            g.campaign.state                    = g.campaign.state or Campaign.State.ACTIVE

            try:
                is_new = False if g.campaign.id else True
                Session.add(g.campaign)
                Session.flush()
                Session.commit()

                if is_new:
                    return redirect("/adv/campaign/%s/media" % g.campaign.id)
                else:
                    return redirect("/adv/campaign/")
            except:
                capp.logger.exception('EXCEPTION IN /adv/campaign/form')

        g.categories = Category.All()

        g.nodes = generateNodes()

        return render_template('/adv/campaign/form.mako', form=form)


    @Route("/<int:campaign_id>/activate")
    def activate(self, campaign_id):
        campaign = Campaign.Key(campaign_id)

        if campaign.account != account.id:
            raise NotOwnedException

        campaign.state = Campaign.State.ACTIVE

        try:
            Session.add(campaign)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("While activating campaign")

        return redirect("/adv/campaign/")

    @Route("/<int:campaign_id>/deactivate")
    def deactivate(self, campaign_id):
        campaign = Campaign.Key(campaign_id)

        if campaign.account != account.id:
            raise NotOwnedException

        campaign.state = Campaign.State.DISABLED

        try:
            Session.add(campaign)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("While deactivating campaign")

        return redirect("/adv/campaign/")

    @Route("/<int:cmp_id>/delete")
    def delete(self, cmp_id):
        campaign = Campaign.Key(cmp_id)

        if campaign.account != account.id:
            raise NotOwnedException

        try:
            Session.delete(campaign)
            Session.flush()
            Session.commit()
        except:
            capp.logger.exception("While deleting campaign")

        return redirect("/adv/campaign/")

    @Route("/forecast", methods=["POST"])
    def forecast(self):
        targeting = reduce(lambda acc, v: acc | int(v), # ["2", "4", ...] -> 2 | 4 | ...
                           request.values.getlist('targeting'),
                           0)

        target_categories       = map(int, request.values.getlist('target_categories'))   # ["1", "5", ...] -> [1, 5, ...]
        target_platforms        = map(int, request.values.getlist('target_platforms'))
        target_devclasses       = map(int, request.values.getlist('target_devclasses'))
        target_content_ratings  = map(int, request.values.getlist('target_content_ratings'))
        target_geos             = map(int, request.values.getlist('target_geos'))

        bid = int(Decimal(request.values.get('bid') or 0) * 10)

        result = { "min"         : 0
                 , "recommended" : 0
                 , "forecast"    : 0
                 }

        result["min"] = CampaignController.BASEBID * (1 + len(request.values.getlist('targeting')) * 0.1)

        # CONSTRUCT QUERY

        q = Session.query(ForecastStat.cost_index,
                          (sqlsum(ForecastStat.impressions)).label('impressions'))

        if targeting & Campaign.Targeting.CATEGORY:
            q = q.filter(and_( ForecastStat.category.in_(target_categories) 
                             , ForecastStat.content_rating.in_(target_content_ratings) ))

        if targeting & Campaign.Targeting.DEVCLASS:
            q = q.filter(ForecastStat.devclass.in_(target_devclasses))

        if targeting & Campaign.Targeting.PLATFORM:
            q = q.filter(ForecastStat.platform.in_(target_platforms))

        q = q.group_by(ForecastStat.cost_index)
        q = q.order_by(asc(ForecastStat.cost_index))

        data = q.all()

        if data:
            total = sum([imps for (idx, imps) in data])

            result["recommended"] = "%.2f" % (sum([(1.0 * idx * imps) / total for (idx, imps) in data]) / 10.0)
            result["forecast"] = sum([imps for (idx, imps) in data if idx <= bid])

        return jsonify(result)
    


















